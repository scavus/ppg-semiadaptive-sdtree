/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob
    Copyright (c) 2017 by ETH Zurich, Thomas Mueller.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/core/plugin.h>
#include <mitsuba/core/sse.h>
#include <mitsuba/core/statistics.h>
#include <mitsuba/render/renderproc.h>
#include <mitsuba/render/scene.h>

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <array>
#include <unordered_map>
#include <atomic>
#include <chrono>
#include <fstream>
#include <functional>
#include <iomanip>
#include <sstream>
#include <immintrin.h>

MTS_NAMESPACE_BEGIN

struct IntegratorStats {
    float runtime;
    size_t spp;
    float avgPathLength;
    float percGuiding;
    float percNonzero;
    float totDtBuildProd;
    float avgDtBuildProd;

    void serialize(const std::string &filename) {
        boost::property_tree::ptree root;
        root.put("runtime", runtime);
        root.put("spp", spp);
        root.put("avg_path_length", avgPathLength);
        root.put("perc_guiding", percGuiding);
        root.put("perc_nonzero", percNonzero);
        root.put("tot_dt_build_prod", totDtBuildProd);
        root.put("avg_dt_build_prod", avgDtBuildProd);
        boost::property_tree::write_json(filename, root);
    }
};

class BlobWriter {
public:
    BlobWriter(const std::string& filename)
        : f(filename, std::ios::out | std::ios::binary) {
    }

    template <typename Type>
    typename std::enable_if<std::is_standard_layout<Type>::value, BlobWriter&>::type
        operator << (Type Element) {
        Write(&Element, 1);
        return *this;
    }

    // CAUTION: This function may break down on big-endian architectures.
    //          The ordering of bytes has to be reverted then.
    template <typename T>
    void Write(T* Src, size_t Size) {
        f.write(reinterpret_cast<const char*>(Src), Size * sizeof(T));
    }

private:
    std::ofstream f;
};

static void addToAtomicFloat(std::atomic<Float>& var, Float val) {
    auto current = var.load();
    while (!var.compare_exchange_weak(current, current + val));
}

inline Float logistic(Float x) {
    return 1 / (1 + std::exp(-x));
}

// Implements the stochastic-gradient-based Adam optimizer [Kingma and Ba 2014]
class AdamOptimizer {
public:
    AdamOptimizer(Float learningRate, int batchSize = 1, Float epsilon = 1e-08f, Float beta1 = 0.9f, Float beta2 = 0.999f) {
		m_hparams = { learningRate, batchSize, epsilon, beta1, beta2 };
	}

    AdamOptimizer& operator=(const AdamOptimizer& arg) {
        m_state = arg.m_state;
        m_hparams = arg.m_hparams;
        return *this;
    }

    AdamOptimizer(const AdamOptimizer& arg) {
        *this = arg;
    }

    void append(Float gradient, Float statisticalWeight) {
        m_state.batchGradient += gradient * statisticalWeight;
        m_state.batchAccumulation += statisticalWeight;

        if (m_state.batchAccumulation > m_hparams.batchSize) {
            step(m_state.batchGradient / m_state.batchAccumulation);

            m_state.batchGradient = 0;
            m_state.batchAccumulation = 0;
        }
    }

    void step(Float gradient) {
        ++m_state.iter;

        Float actualLearningRate = m_hparams.learningRate * std::sqrt(1 - std::pow(m_hparams.beta2, m_state.iter)) / (1 - std::pow(m_hparams.beta1, m_state.iter));
        m_state.firstMoment = m_hparams.beta1 * m_state.firstMoment + (1 - m_hparams.beta1) * gradient;
        m_state.secondMoment = m_hparams.beta2 * m_state.secondMoment + (1 - m_hparams.beta2) * gradient * gradient;
        m_state.variable -= actualLearningRate * m_state.firstMoment / (std::sqrt(m_state.secondMoment) + m_hparams.epsilon);

        // Clamp the variable to the range [-20, 20] as a safeguard to avoid numerical instability:
        // since the sigmoid involves the exponential of the variable, value of -20 or 20 already yield
        // in *extremely* small and large results that are pretty much never necessary in practice.
        m_state.variable = std::min(std::max(m_state.variable, -20.0f), 20.0f);
    }

    Float variable() const {
        return m_state.variable;
    }

private:
    struct State {
        int iter = 0;
        Float firstMoment = 0;
        Float secondMoment = 0;
        Float variable = 0;

        Float batchAccumulation = 0;
        Float batchGradient = 0;
    } m_state;

    struct Hyperparameters {
        Float learningRate;
        int batchSize;
        Float epsilon;
        Float beta1;
        Float beta2;
    } m_hparams;
};

enum class ESampleCombination {
    EDiscard,
    EDiscardWithAutomaticBudget,
    EInverseVariance,
};

enum class EBsdfSamplingFractionLoss {
    ENone,
    EKL,
    EVariance,
};

enum class ESpatialFilter {
    ENearest,
    EStochasticBox,
    EBox,
};

enum class EDirectionalFilter {
    ENearest,
    EBox,
};

enum class EBsdfLutType {
    ENone,
    EWorldSpace,
    EObjectSpace,
};

static std::unordered_map<std::string, Float***> g_worldspaceBsdfLutMap;
static std::unordered_map<std::string, Float**> g_objectspaceBsdfLutMap;

static EBsdfLutType g_bsdfLutType = EBsdfLutType::EWorldSpace;
static bool g_enableWorldspaceLutCompression = false;
static bool g_enableSimd = false;
const static size_t g_altSideLength = 32;
const static size_t g_altCellCount = g_altSideLength * g_altSideLength;
const static Float g_altCellSize = 1.0f / static_cast<Float>(g_altSideLength);
static size_t g_defaultGridSideLength = 4;
static Float g_defaultCellSize = 1.0f / static_cast<Float>(g_defaultGridSideLength);
static size_t g_bsdfLutSampleCount = 4;
static Vector* g_cellIdxToWorldLut = nullptr;

void precomputeBsdfs(const Scene* scene, const size_t lutGridSideLength) {
    const auto shapes = scene->getShapes();
    const auto meshes = scene->getMeshes();

    std::unordered_map<std::string, BSDF*> bsdfMap;

    for (auto shape : shapes) {
        if (shape->hasBSDF()) {
            BSDF* bsdf = shape->getBSDF();
            std::string bsdfId = bsdf->getID();
            bsdfMap.insert({bsdfId, bsdf});
        }
    }

    printf("There are %ld unique BSDFs in the scene.\n", bsdfMap.size());

    const size_t lutCellCount = lutGridSideLength * lutGridSideLength;
    const Float lutCellSize = 1.0f / static_cast<Float>(lutGridSideLength);

    auto cellIdxTo2d = [&](const size_t cidx, const size_t gridSideLength) {
        return Point2u(cidx % gridSideLength, std::floor(cidx / gridSideLength));
    };

    auto cellIdxToMidpoint = [&](const size_t cidx, const size_t gridSideLength, const Float cellSize) {
        const Point2u c2d = cellIdxTo2d(cidx, gridSideLength);
        Point2 midpoint = Point2(static_cast<Float>(c2d.x) * cellSize, static_cast<Float>(c2d.y) * cellSize) + cellSize * Point2(0.5f, 0.5f);
        return midpoint;
    };

    auto canonicalToEuclidean = [](const Point2& p) {
        const Float cosTheta = 2 * p.x - 1;
        const Float phi = 2 * M_PI * p.y;

        const Float sinTheta = sqrt(1 - cosTheta * cosTheta);
        Float sinPhi, cosPhi;
        math::sincos(phi, &sinPhi, &cosPhi);

        return Vector{sinTheta * cosPhi, sinTheta * sinPhi, cosTheta};
    };

    if ((g_bsdfLutType == EBsdfLutType::ENone) || (g_bsdfLutType == EBsdfLutType::EObjectSpace)) {
        g_cellIdxToWorldLut = new Vector[lutCellCount];

        for (size_t i = 0; i < lutCellCount; i++) {
            const auto wCanonical = cellIdxToMidpoint(i, lutGridSideLength, lutCellSize);
            const auto wEuclidean = canonicalToEuclidean(wCanonical);
            g_cellIdxToWorldLut[i] = wEuclidean;
        }
    }

    auto cellIdxToOrigin = [&](const size_t cidx, const size_t gridSideLength, const Float cellSize) {
        const Point2u c2d = cellIdxTo2d(cidx, gridSideLength);
        return Point2(static_cast<Float>(c2d.x) * cellSize, static_cast<Float>(c2d.y) * cellSize);
    };

    auto getTransform = [](const Vector& u, const Vector& t) {
        const auto v = normalize(cross(u, t));
        const auto w = normalize(cross(v, u));
        const auto tnb = Matrix3x3(u, w, v);

        const auto alpha = dot(u, t);
        const auto beta = dot(w, t);
        const auto rot = Matrix3x3(alpha, -beta, 0, beta, alpha, 0, 0, 0, 1);

        Matrix3x3 tnbTransposed;
        tnb.transpose(tnbTransposed);
        return tnb * rot * tnbTransposed;
    };

    if (g_bsdfLutType == EBsdfLutType::EObjectSpace) {
        const size_t bsdfLutSampleCount = g_bsdfLutSampleCount;
        const size_t bsdfSampleCountSq = bsdfLutSampleCount * bsdfLutSampleCount;

        for (const auto &m : bsdfMap) {
            const auto bsdfId = m.first;
            const auto bsdf = m.second;

            printf("Precomputing object space BSDF LUT for %s\n", bsdfId.c_str());
            auto bsdfLut = new Float*[g_altCellCount];
            // #pragma omp parallel for
            for (uint32_t i = 0; i < g_altCellCount; i++) {
                bsdfLut[i] = new Float[lutCellCount];
                const Point2 wiCanonical = cellIdxToMidpoint(i, g_altSideLength, g_altCellSize);
                const Vector wiEuclidean = canonicalToEuclidean(wiCanonical);

                for (uint32_t j = 0; j < lutCellCount; j++) {
                    if (bsdfLutSampleCount > 1) {
                        Float wBsdfTotal = 0.0f;
                        // stratifiedSample2D(rnd, bsdfSamples, bsdfSampleCount, bsdfSampleCount, true);
                        const Point2 cellOrigin = cellIdxToOrigin(j, lutGridSideLength, lutCellSize);
                        for (size_t n = 0; n < bsdfSampleCountSq; n++) {
                            // const Point2 woCanonical = cellOrigin + lutCellSize * bsdfSamples[n];
                            const Point2 woCanonical = cellOrigin + lutCellSize * cellIdxToMidpoint(n, bsdfLutSampleCount, 1.0f / static_cast<Float>(bsdfLutSampleCount));
                            const Vector woEuclidean = canonicalToEuclidean(woCanonical);
                            Intersection its;
                            BSDFSamplingRecord brec(its, wiEuclidean, woEuclidean, ERadiance);
                            wBsdfTotal += bsdf->eval(brec).average();
                        }

                        bsdfLut[i][j] = wBsdfTotal / static_cast<Float>(bsdfSampleCountSq);
                    } else {
                        const Point2 woCanonical = cellIdxToMidpoint(j, lutGridSideLength, lutCellSize);
                        const Vector woEuclidean = canonicalToEuclidean(woCanonical);
                        Intersection its;
                        BSDFSamplingRecord brec(its, wiEuclidean, woEuclidean, ERadiance);
                        bsdfLut[i][j] = bsdf->eval(brec).average();
                    }
                }

            }

            g_objectspaceBsdfLutMap.insert({bsdfId, bsdfLut});

        }
    }

    if (g_bsdfLutType == EBsdfLutType::EWorldSpace) {
        // auto rnd = new mitsuba::Random(1);
        const size_t bsdfLutSampleCount = g_bsdfLutSampleCount;
        const size_t bsdfSampleCountSq = bsdfLutSampleCount * bsdfLutSampleCount;
        auto bsdfSamples = new Point2[bsdfSampleCountSq];

        for (const auto &m : bsdfMap) {
            const auto bsdfId = m.first;
            const auto bsdf = m.second;
            if (!g_enableWorldspaceLutCompression) {
                printf("Precomputing world space BSDF LUT for %s\n", bsdfId.c_str());
                auto bsdfLut = new Float**[g_altCellCount];
                #pragma omp parallel for
                for (uint32_t i = 0; i < g_altCellCount; i++) {
                    bsdfLut[i] = new Float*[g_altCellCount];
                    const Point2 nCanonical = cellIdxToMidpoint(i, g_altSideLength, g_altCellSize);
                    const Vector nEuclidean = canonicalToEuclidean(nCanonical);
                    const Matrix3x3 xform = getTransform(nEuclidean, Vector(0.0f, 0.0f, 1.0f));

                    for (uint32_t j = 0; j < g_altCellCount; j++) {
                        bsdfLut[i][j] = new Float[lutCellCount];
                        // bsdfLut[i][j] = (Float*)aligned_alloc(32, sizeof(Float) * lutCellCount);
                        const Point2 wiCanonical = cellIdxToMidpoint(j, g_altSideLength, g_altCellSize);
                        const Vector wiEuclidean = canonicalToEuclidean(wiCanonical);
                        const Vector wiLocal = xform * wiEuclidean;

                        for (uint32_t k = 0; k < lutCellCount; k++) {
                            if (bsdfLutSampleCount > 1) {
                                Float wBsdfTotal = 0.0f;
                                // stratifiedSample2D(rnd, bsdfSamples, bsdfSampleCount, bsdfSampleCount, true);
                                const Point2 cellOrigin = cellIdxToOrigin(k, lutGridSideLength, lutCellSize);
                                for (size_t n = 0; n < bsdfSampleCountSq; n++) {
                                    // const Point2 woCanonical = cellOrigin + lutCellSize * bsdfSamples[n];
                                    const Point2 woCanonical = cellOrigin + lutCellSize * cellIdxToMidpoint(n, bsdfLutSampleCount, 1.0f / static_cast<Float>(bsdfLutSampleCount));
                                    const Vector woEuclidean = canonicalToEuclidean(woCanonical);
                                    const Vector woLocal = xform * woEuclidean;
                                    Intersection its;
                                    BSDFSamplingRecord brec(its, wiLocal, woLocal, ERadiance);
                                    wBsdfTotal += bsdf->eval(brec).average();
                                }

                                bsdfLut[i][j][k] = wBsdfTotal / static_cast<Float>(bsdfSampleCountSq);
                            } else {
                                const Point2 woCanonical = cellIdxToMidpoint(k, lutGridSideLength, lutCellSize);
                                const Vector woEuclidean = canonicalToEuclidean(woCanonical);
                                const Vector woLocal = xform * woEuclidean;
                                Intersection its;
                                BSDFSamplingRecord brec(its, wiLocal, woLocal, ERadiance);
                                bsdfLut[i][j][k] = bsdf->eval(brec).average();
                            }
                        }
                    }
                }

                g_worldspaceBsdfLutMap.insert({bsdfId, bsdfLut});
            } else {
                printf("Precomputing compressed world space BSDF LUT for %s\n", bsdfId.c_str());
                auto bsdfLut_ = new Float**[(g_altSideLength / 2) * (g_altSideLength / lutGridSideLength)];
#pragma omp parallel for
                for (uint32_t ii_x = 0; ii_x < g_altSideLength / 2; ii_x++) {
                    for (uint32_t ii_y = 0; ii_y < g_altSideLength / lutGridSideLength; ii_y++) {
                        const auto i = (g_altSideLength / 2) * ii_y + ii_x;
                        SAssert(i >= 0 && i < ((g_altSideLength / 2) * (g_altSideLength / lutGridSideLength)));
                        bsdfLut_[i] = new Float*[g_altCellCount];
                        const Point2 nCanonical = Point2(static_cast<Float>(ii_x) * g_altCellSize, static_cast<Float>(ii_y) * g_altCellSize) + g_altCellSize * Point2(0.5f, 0.5f);
                        const Vector nEuclidean = canonicalToEuclidean(nCanonical);
                        const Matrix3x3 xform = getTransform(nEuclidean, Vector(0.0f, 0.0f, 1.0f));

                        for (uint32_t j = 0; j < g_altCellCount; j++) {
                            bsdfLut_[i][j] = new Float[lutCellCount];
                            const Point2 wiCanonical = cellIdxToMidpoint(j, g_altSideLength, g_altCellSize);
                            const Vector wiEuclidean = canonicalToEuclidean(wiCanonical);
                            const Vector wiLocal = xform * wiEuclidean;
                            for (uint32_t k = 0; k < lutCellCount; k++) {
                                if (bsdfLutSampleCount > 1) {
                                    Float wBsdfTotal = 0.0f;
                                    // stratifiedSample2D(rnd, bsdfSamples, bsdfSampleCount, bsdfSampleCount, true);
                                    const Point2 cellOrigin = cellIdxToOrigin(k, lutGridSideLength, lutCellSize);
                                    for (size_t n = 0; n < bsdfSampleCountSq; n++) {

                                        const Point2 woCanonical = cellOrigin + lutCellSize * cellIdxToMidpoint(n, bsdfLutSampleCount, 1.0f / static_cast<Float>(bsdfLutSampleCount));
                                        const Vector woEuclidean = canonicalToEuclidean(woCanonical);
                                        const Vector woLocal = xform * woEuclidean;
                                        Intersection its;
                                        BSDFSamplingRecord brec(its, wiLocal, woLocal, ERadiance);
                                        wBsdfTotal += bsdf->eval(brec).average();
                                    }

                                    bsdfLut_[i][j][k] = wBsdfTotal / static_cast<Float>(bsdfSampleCountSq);
                                } else {
                                    const Point2 woCanonical = cellIdxToMidpoint(k, lutGridSideLength, lutCellSize);
                                    const Vector woEuclidean = canonicalToEuclidean(woCanonical);
                                    const Vector woLocal = xform * woEuclidean;
                                    Intersection its;
                                    BSDFSamplingRecord brec(its, wiLocal, woLocal, ERadiance);
                                    bsdfLut_[i][j][k] = bsdf->eval(brec).average();
                                }
                            }
                        }
                    }
                }

                g_worldspaceBsdfLutMap.insert({bsdfId, bsdfLut_});
            }
        }

        delete[] bsdfSamples;
    }
}

class QuadTreeNode {
public:
    QuadTreeNode() {
        m_children = {};
        for (size_t i = 0; i < m_sum.size(); ++i) {
            m_sum[i].store(0, std::memory_order_relaxed);
        }
    }

    void setSum(int index, Float val) {
        m_sum[index].store(val, std::memory_order_relaxed);
    }

    Float sum(int index) const {
        return m_sum[index].load(std::memory_order_relaxed);
    }

    void copyFrom(const QuadTreeNode& arg) {
        for (int i = 0; i < 4; ++i) {
            setSum(i, arg.sum(i));
            m_children[i] = arg.m_children[i];
        }
    }

    QuadTreeNode(const QuadTreeNode& arg) {
        copyFrom(arg);
    }

    QuadTreeNode& operator=(const QuadTreeNode& arg) {
        copyFrom(arg);
        return *this;
    }

    void setChild(int idx, uint16_t val) {
        m_children[idx] = val;
    }

    uint16_t child(int idx) const {
        return m_children[idx];
    }

    void setSum(Float val) {
        for (int i = 0; i < 4; ++i) {
            setSum(i, val);
        }
    }

    int childIndex(Point2& p) const {
        int res = 0;
        for (int i = 0; i < Point2::dim; ++i) {
            if (p[i] < 0.5f) {
                p[i] *= 2;
            } else {
                p[i] = (p[i] - 0.5f) * 2;
                res |= 1 << i;
            }
        }

        return res;
    }

    // Evaluates the directional irradiance *sum density* (i.e. sum / area) at a given location p.
    // To obtain radiance, the sum density (result of this function) must be divided
    // by the total statistical weight of the estimates that were summed up.
    Float eval(Point2& p, const std::vector<QuadTreeNode>& nodes) const {
        SAssert(p.x >= 0 && p.x <= 1 && p.y >= 0 && p.y <= 1);
        const int index = childIndex(p);
        if (isLeaf(index)) {
            return 4 * sum(index);
        } else {
            return 4 * nodes[child(index)].eval(p, nodes);
        }
    }

    Float pdf(Point2& p, const std::vector<QuadTreeNode>& nodes) const {
        SAssert(p.x >= 0 && p.x <= 1 && p.y >= 0 && p.y <= 1);
        const int index = childIndex(p);
        if (!(sum(index) > 0)) {
            return 0;
        }

        const Float factor = 4 * sum(index) / (sum(0) + sum(1) + sum(2) + sum(3));
        if (isLeaf(index)) {
            return factor;
        } else {
            return factor * nodes[child(index)].pdf(p, nodes);
        }
    }

    int depthAt(Point2& p, const std::vector<QuadTreeNode>& nodes) const {
        SAssert(p.x >= 0 && p.x <= 1 && p.y >= 0 && p.y <= 1);
        const int index = childIndex(p);
        if (isLeaf(index)) {
            return 1;
        } else {
            return 1 + nodes[child(index)].depthAt(p, nodes);
        }
    }

    Point2 sample(Sampler* sampler, const std::vector<QuadTreeNode>& nodes) const {
        int index = 0;

        Float topLeft = sum(0);
        Float topRight = sum(1);
        Float partial = topLeft + sum(2);
        Float total = partial + topRight + sum(3);

        // Should only happen when there are numerical instabilities.
        if (!(total > 0.0f)) {
            return sampler->next2D();
        }

        Float boundary = partial / total;
        Point2 origin = Point2{0.0f, 0.0f};

        Float sample = sampler->next1D();

        if (sample < boundary) {
            SAssert(partial > 0);
            sample /= boundary;
            boundary = topLeft / partial;
        } else {
            partial = total - partial;
            SAssert(partial > 0);
            origin.x = 0.5f;
            sample = (sample - boundary) / (1.0f - boundary);
            boundary = topRight / partial;
            index |= 1 << 0;
        }

        if (sample < boundary) {
            sample /= boundary;
        } else {
            origin.y = 0.5f;
            sample = (sample - boundary) / (1.0f - boundary);
            index |= 1 << 1;
        }

        if (isLeaf(index)) {
            return origin + 0.5f * sampler->next2D();
        } else {
            return origin + 0.5f * nodes[child(index)].sample(sampler, nodes);
        }
    }

    void record(Point2& p, Float irradiance, std::vector<QuadTreeNode>& nodes) {
        SAssert(p.x >= 0 && p.x <= 1 && p.y >= 0 && p.y <= 1);
        int index = childIndex(p);

        if (isLeaf(index)) {
            addToAtomicFloat(m_sum[index], irradiance);
        } else {
            nodes[child(index)].record(p, irradiance, nodes);
        }
    }

    void record(Point2& p, Float irradiance, const Point2 quadtreeOrigin, const Float quadtreeScale, std::vector<QuadTreeNode>& nodes) {
        SAssert(p.x >= 0 && p.x <= 1 && p.y >= 0 && p.y <= 1);
        int index = childIndex(p);

        if (isLeaf(index)) {
            addToAtomicFloat(m_sum[index], irradiance);
        } else {
            nodes[child(index)].record(p, irradiance, quadtreeOrigin, quadtreeScale, nodes);
        }
    }

    Float computeOverlappingArea(const Point2& min1, const Point2& max1, const Point2& min2, const Point2& max2) {
        Float lengths[2];
        for (int i = 0; i < 2; ++i) {
            lengths[i] = std::max(std::min(max1[i], max2[i]) - std::max(min1[i], min2[i]), 0.0f);
        }
        return lengths[0] * lengths[1];
    }

    void record(const Point2& origin, Float size, Point2 nodeOrigin, Float nodeSize, Float value, std::vector<QuadTreeNode>& nodes) {
        Float childSize = nodeSize / 2;
        for (int i = 0; i < 4; ++i) {
            Point2 childOrigin = nodeOrigin;
            if (i & 1) { childOrigin[0] += childSize; }
            if (i & 2) { childOrigin[1] += childSize; }

            Float w = computeOverlappingArea(origin, origin + Point2(size), childOrigin, childOrigin + Point2(childSize));
            if (w > 0.0f) {
                if (isLeaf(i)) {
                    addToAtomicFloat(m_sum[i], value * w);
                } else {
                    nodes[child(i)].record(origin, size, childOrigin, childSize, value, nodes);
                }
            }
        }
    }

    bool isLeaf(int index) const {
        return child(index) == 0;
    }

    // Ensure that each quadtree node's sum of irradiance estimates
    // equals that of all its children.
    void build(std::vector<QuadTreeNode>& nodes) {
        for (int i = 0; i < 4; ++i) {
            // During sampling, all irradiance estimates are accumulated in
            // the leaves, so the leaves are built by definition.
            if (isLeaf(i)) {
                continue;
            }

            QuadTreeNode& c = nodes[child(i)];

            // Recursively build each child such that their sum becomes valid...
            c.build(nodes);

            // ...then sum up the children's sums.
            Float sum = 0;
            for (int j = 0; j < 4; ++j) {
                sum += c.sum(j);
            }
            setSum(i, sum);
        }
    }

private:
    std::array<std::atomic<Float>, 4> m_sum;
    std::array<uint16_t, 4> m_children;
};

class DTree {
public:
    DTree() {
        m_atomic.sum.store(0, std::memory_order_relaxed);
        m_maxDepth = 0;
        m_nodes.emplace_back();
        m_nodes.front().setSum(0.0f);
    }

    const QuadTreeNode& node(size_t i) const {
        return m_nodes[i];
    }

    Float mean() const {
        if (m_atomic.statisticalWeight == 0) {
            return 0;
        }
        const Float factor = 1 / (M_PI * 4 * m_atomic.statisticalWeight);
        return factor * m_atomic.sum;
    }

    void recordIrradiance(Point2 p, Float irradiance, Float statisticalWeight, EDirectionalFilter directionalFilter) {
        if (std::isfinite(statisticalWeight) && statisticalWeight > 0) {
            addToAtomicFloat(m_atomic.statisticalWeight, statisticalWeight);

            if (std::isfinite(irradiance) && irradiance > 0) {
                if (directionalFilter == EDirectionalFilter::ENearest) {
                    m_nodes[0].record(p, irradiance * statisticalWeight, m_nodes);
                } else {
                    int depth = depthAt(p);
                    Float size = std::pow(0.5f, depth);

                    Point2 origin = p;
                    origin.x -= size / 2;
                    origin.y -= size / 2;
                    m_nodes[0].record(origin, size, Point2(0.0f), 1.0f, irradiance * statisticalWeight / (size * size), m_nodes);
                }
            }
        }
    }

    Float pdf(Point2 p) const {
        if (!(mean() > 0)) {
            return 1 / (4 * M_PI);
        }

        return m_nodes[0].pdf(p, m_nodes) / (4 * M_PI);
    }

    int depthAt(Point2 p) const {
        return m_nodes[0].depthAt(p, m_nodes);
    }

    int depth() const {
        return m_maxDepth;
    }

    Point2 sample(Sampler* sampler) const {
        if (!(mean() > 0)) {
            return sampler->next2D();
        }

        Point2 res = m_nodes[0].sample(sampler, m_nodes);

        res.x = math::clamp(res.x, 0.0f, 1.0f);
        res.y = math::clamp(res.y, 0.0f, 1.0f);

        return res;
    }

    size_t numNodes() const {
        return m_nodes.size();
    }

    Float statisticalWeight() const {
        return m_atomic.statisticalWeight;
    }

    void setStatisticalWeight(Float statisticalWeight) {
        m_atomic.statisticalWeight = statisticalWeight;
    }

    void reset(const DTree& previousDTree, int newMaxDepth, Float subdivisionThreshold) {
        m_atomic = Atomic{};
        m_maxDepth = 0;
        m_nodes.clear();
        m_nodes.emplace_back();

        struct StackNode {
            size_t nodeIndex;
            size_t otherNodeIndex;
            const DTree* otherDTree;
            int depth;
        };

        std::stack<StackNode> nodeIndices;
        nodeIndices.push({0, 0, &previousDTree, 1});

        const Float total = previousDTree.m_atomic.sum;

        // Create the topology of the new DTree to be the refined version
        // of the previous DTree. Subdivision is recursive if enough energy is there.
        while (!nodeIndices.empty()) {
            StackNode sNode = nodeIndices.top();
            nodeIndices.pop();

            m_maxDepth = std::max(m_maxDepth, sNode.depth);

            for (int i = 0; i < 4; ++i) {
                const QuadTreeNode& otherNode = sNode.otherDTree->m_nodes[sNode.otherNodeIndex];
                const Float fraction = total > 0 ? (otherNode.sum(i) / total) : std::pow(0.25f, sNode.depth);
                SAssert(fraction <= 1.0f + Epsilon);

                if (sNode.depth < newMaxDepth && fraction > subdivisionThreshold) {
                    if (!otherNode.isLeaf(i)) {
                        SAssert(sNode.otherDTree == &previousDTree);
                        nodeIndices.push({m_nodes.size(), otherNode.child(i), &previousDTree, sNode.depth + 1});
                    } else {
                        nodeIndices.push({m_nodes.size(), m_nodes.size(), this, sNode.depth + 1});
                    }

                    m_nodes[sNode.nodeIndex].setChild(i, static_cast<uint16_t>(m_nodes.size()));
                    m_nodes.emplace_back();
                    m_nodes.back().setSum(otherNode.sum(i) / 4);

                    if (m_nodes.size() > std::numeric_limits<uint16_t>::max()) {
                        SLog(EWarn, "DTreeWrapper hit maximum children count.");
                        nodeIndices = std::stack<StackNode>();
                        break;
                    }
                }
            }
        }

        // Uncomment once memory becomes an issue.
        //m_nodes.shrink_to_fit();

        for (auto& node : m_nodes) {
            node.setSum(0);
        }
    }

    void reset(const DTree& previousDTree, int newMaxDepth, Float subdivisionThreshold, const Float total) {
        m_atomic = Atomic{};
        m_maxDepth = 0;
        m_nodes.clear();
        m_nodes.emplace_back();

        struct StackNode {
            size_t nodeIndex;
            size_t otherNodeIndex;
            const DTree* otherDTree;
            int depth;
        };

        std::stack<StackNode> nodeIndices;
        nodeIndices.push({0, 0, &previousDTree, 1});

        // Create the topology of the new DTree to be the refined version
        // of the previous DTree. Subdivision is recursive if enough energy is there.
        while (!nodeIndices.empty()) {
            StackNode sNode = nodeIndices.top();
            nodeIndices.pop();

            m_maxDepth = std::max(m_maxDepth, sNode.depth);

            for (int i = 0; i < 4; ++i) {
                const QuadTreeNode& otherNode = sNode.otherDTree->m_nodes[sNode.otherNodeIndex];
                // const Float fraction = total > 0 ? (otherNode.sum(i) / total) : std::pow(0.25f, sNode.depth);
                const Float fraction = total > 0 ? (otherNode.sum(i) / total) : 0;
                SAssert(fraction <= 1.0f + Epsilon);

                if (sNode.depth < newMaxDepth && fraction > subdivisionThreshold) {
                    if (!otherNode.isLeaf(i)) {
                        SAssert(sNode.otherDTree == &previousDTree);
                        nodeIndices.push({m_nodes.size(), otherNode.child(i), &previousDTree, sNode.depth + 1});
                    } else {
                        nodeIndices.push({m_nodes.size(), m_nodes.size(), this, sNode.depth + 1});
                    }

                    m_nodes[sNode.nodeIndex].setChild(i, static_cast<uint16_t>(m_nodes.size()));
                    m_nodes.emplace_back();
                    m_nodes.back().setSum(otherNode.sum(i) / 4);

                    if (m_nodes.size() > std::numeric_limits<uint16_t>::max()) {
                        SLog(EWarn, "DTreeWrapper hit maximum children count.");
                        nodeIndices = std::stack<StackNode>();
                        break;
                    }
                }
            }
        }

        // Uncomment once memory becomes an issue.
        //m_nodes.shrink_to_fit();

        for (auto& node : m_nodes) {
            node.setSum(0);
        }
    }

    size_t approxMemoryFootprint() const {
        return m_nodes.capacity() * sizeof(QuadTreeNode) + sizeof(*this);
    }

    void build() {
        auto& root = m_nodes[0];

        // Build the quadtree recursively, starting from its root.
        root.build(m_nodes);

        // Ensure that the overall sum of irradiance estimates equals
        // the sum of irradiance estimates found in the quadtree.
        Float sum = 0;
        for (int i = 0; i < 4; ++i) {
            sum += root.sum(i);
        }
        m_atomic.sum.store(sum);
    }

private:
    std::vector<QuadTreeNode> m_nodes;

    struct Atomic {
        Atomic() {
            sum.store(0, std::memory_order_relaxed);
            statisticalWeight.store(0, std::memory_order_relaxed);
        }

        Atomic(const Atomic& arg) {
            *this = arg;
        }

        Atomic& operator=(const Atomic& arg) {
            sum.store(arg.sum.load(std::memory_order_relaxed), std::memory_order_relaxed);
            statisticalWeight.store(arg.statisticalWeight.load(std::memory_order_relaxed), std::memory_order_relaxed);
            return *this;
        }

        std::atomic<Float> sum;
        std::atomic<Float> statisticalWeight;

    } m_atomic;

    int m_maxDepth;
};

struct HashGridCell {
    HashGridCell() {
        sum.store(0, std::memory_order_relaxed);
    }

    HashGridCell(const HashGridCell& arg) {
        sum.store(arg.sum.load(std::memory_order_relaxed), std::memory_order_relaxed);
        quadtree = arg.quadtree;
    }

    HashGridCell& operator=(const HashGridCell& arg) {
        sum.store(arg.sum.load(std::memory_order_relaxed), std::memory_order_relaxed);
        quadtree = arg.quadtree;
        return *this;
    }

    std::atomic<Float> sum;

    DTree quadtree;
};

class HashGrid {

public:
    HashGrid() {
        m_atomic.sum.store(0, std::memory_order_relaxed);
        m_atomic.statisticalWeight.store(0, std::memory_order_relaxed);

        m_cellSize = g_defaultCellSize;
        m_minExtent = 0.0f;
        m_maxExtent = 1.0f;

        m_width = (m_maxExtent - m_minExtent) / m_cellSize;
        m_bucketCount = static_cast<size_t>(m_width) * static_cast<size_t>(m_width);
        m_cells.resize(m_bucketCount);
        m_cellSums.resize(m_bucketCount);

        // m_pmf.reserve(m_bucketCount);
        // m_pmf.clear();

        m_pmf.m_cdf.resize(m_bucketCount + 1);
        m_pmf.m_cdf[0] = 0.0f;
        m_pmf.m_normalized = false;
    }

    HashGrid(Float cellSize, Float minExtent, Float maxExtent) {
        m_atomic.sum.store(0, std::memory_order_relaxed);
        m_atomic.statisticalWeight.store(0, std::memory_order_relaxed);
        m_cellSize = cellSize;
        m_minExtent = minExtent;
        m_maxExtent = maxExtent;

        m_width = (maxExtent - minExtent) / cellSize;
        m_bucketCount = static_cast<size_t>(m_width) * static_cast<size_t>(m_width);
        m_cells.resize(m_bucketCount);
        m_cellSums.resize(m_bucketCount);
        m_pmf.reserve(m_bucketCount);
        m_pmf.clear();

        m_pmf.m_cdf.resize(m_bucketCount + 1);
        m_pmf.m_cdf[0] = 0.0f;
        m_pmf.m_normalized = false;
    }

    Float cellSize() const {
        return m_cellSize;
    }

    inline size_t cellIdx(const Point2& p) const {
        size_t cidx = static_cast<size_t>(std::floor(std::min(1.0f - 1e-3f, p.x) / m_cellSize) + std::floor(std::min(1.0f - 1e-3f, p.y) / m_cellSize) * m_width);

        // HACK: Hash function is problematic on boundary
        cidx = std::min(cidx, (m_bucketCount - 1));
        return cidx;
    }

    inline size_t cellIdx(const Point2& p, const size_t gridSideLength, const Float cellSize, const size_t cellCount) const {
        size_t cidx = static_cast<size_t>(std::floor(std::min(1.0f - 1e-3f, p.x) / cellSize) + std::floor(std::min(1.0f - 1e-3f, p.y) / cellSize) * gridSideLength);

        // HACK: Hash function is problematic on boundary
        cidx = std::min(cidx, (cellCount - 1));
        return cidx;
    }

    Point2u cellIdxTo2d(size_t cidx) const {
        return Point2u(cidx % static_cast<size_t>(m_width), std::floor(cidx / static_cast<size_t>(m_width)));
    }

    Point2 cellIdxToMidpoint(size_t cidx) const {
        Point2u c2d = cellIdxTo2d(cidx);
        Point2 midpoint = Point2(static_cast<Float>(c2d.x) * m_cellSize, static_cast<Float>(c2d.y) * m_cellSize) + m_cellSize * Point2(0.5f, 0.5f);
        // midpoint.x = math::clamp(midpoint.x, 0.0f, 1.0f);
        // midpoint.y = math::clamp(midpoint.y, 0.0f, 1.0f);

        return midpoint;
    }

    Point2 cellIdxToOrigin(size_t cidx) const {
        Point2u c2d = cellIdxTo2d(cidx);
        return Point2(static_cast<Float>(c2d.x) * m_cellSize, static_cast<Float>(c2d.y) * m_cellSize);
        // midpoint.x = math::clamp(midpoint.x, 0.0f, 1.0f);
        // midpoint.y = math::clamp(midpoint.y, 0.0f, 1.0f);
    }

    Float mean() const {
        if (m_atomic.statisticalWeight == 0) {
            return 0;
        }
        const Float factor = 1 / (M_PI * 4 * m_atomic.statisticalWeight);
        return factor * m_atomic.sum;
    }

    void recordIrradiance(Point2 p, Float irradiance, Float statisticalWeight, EDirectionalFilter directionalFilter) {
        const size_t cidx = cellIdx(p);
        const Point2 quadtreeOrigin = cellIdxToOrigin(cidx);
        const Float quadtreeScale = m_cellSize;
        auto& cell = m_cells[cidx];
        const Point2 pQuadtree = (p + -quadtreeOrigin) / quadtreeScale;

        if (std::isfinite(statisticalWeight) && statisticalWeight > 0) {
            addToAtomicFloat(m_atomic.statisticalWeight, statisticalWeight);

            if (std::isfinite(irradiance) && irradiance > 0) {
                addToAtomicFloat(cell.sum, irradiance * statisticalWeight);
            }
        }

        cell.quadtree.recordIrradiance(pQuadtree, irradiance, statisticalWeight, directionalFilter);
    }

    Float pdf(Point2 p) const {
        if (!(mean() > 0)) {
            return 1 / (4 * M_PI);
        }

        const auto cidx = cellIdx(p);
        return (m_pmf[cidx] / ((4 * M_PI) * (m_cellSize * m_cellSize))) * m_cells[cidx].quadtree.pdf(p);
    }

    Float pdfGrid(Point2 p) const {
        return (m_width * m_width * m_pmf[cellIdx(p)]);
    }

    Float pdfQuadtree(Point2 pGrid, Point2 pQuadtree) const {
        return m_cells[cellIdx(pGrid)].quadtree.pdf(pQuadtree);
    }

    int depthAt(Point2 p) const {
        return 0;
    }

    int depth() const {
        return 0;
    }

    Float pmf(const size_t cidx) const {
        // m_pmf.getSum();
        return m_pmf[cidx];
    }

    Float pmfSum() const {
        return m_pmf.getSum();
    }

    Float pmfNorm() const {
        return m_pmf.getNormalization();
    }

    Float cellsum(const size_t cidx) const {
        return m_cells[cidx].sum.load(std::memory_order_relaxed);
    }

    Point2 sample(Sampler* sampler) const {
        if (!(mean() > 0)) {
            return sampler->next2D();
        }

        size_t cidx = m_pmf.sample(sampler->next1D());
        Point2u c2d = cellIdxTo2d(cidx);

        Point2 res = Point2(static_cast<Float>(c2d.x) * m_cellSize, static_cast<Float>(c2d.y) * m_cellSize) + m_cellSize * sampler->next2D();
        res.x = math::clamp(res.x, 0.0f, 1.0f);
        res.y = math::clamp(res.y, 0.0f, 1.0f);

        return res;
    }

    size_t sample_grid(Sampler* sampler) const {
        return m_pmf.sample(sampler->next1D());
    }

    Point2 sample_quadtree(Sampler* sampler, const size_t cidx) const {
        return m_cells[cidx].quadtree.sample(sampler);
    }

    size_t numNodes() const {
        // return m_cells.size();
        size_t totalNodeCount = 0;
        for (const auto& cell : m_cells) {
            totalNodeCount += cell.quadtree.numNodes();
        }

        return totalNodeCount;
    }

    Float sum() const {
        return m_atomic.sum;
    }

    Float statisticalWeight() const {
        return m_atomic.statisticalWeight;
    }

    void setStatisticalWeight(Float statisticalWeight) {
        m_atomic.statisticalWeight = statisticalWeight;
    }

    void div2AllStatisticalWeights() {
        const Float old = m_atomic.statisticalWeight;
        m_atomic.statisticalWeight = old / 2.0f;

        for (auto& cell : m_cells) {
            cell.quadtree.setStatisticalWeight(cell.quadtree.statisticalWeight() / 2.0f);
        }
    }

    void reset(const HashGrid& previousDTree, int newMaxDepth, Float subdivisionThreshold) {
        m_atomic = Atomic{};
        m_pmf.clear();

        for (size_t i = 0; i < m_cells.size(); i++) {
            const auto& previous_cell = previousDTree.m_cells[i];
            m_cellSums[i] = 0;
            auto& cell = m_cells[i];
            cell.sum.store(0, std::memory_order_relaxed);
            cell.quadtree.reset(previous_cell.quadtree, newMaxDepth, subdivisionThreshold, previousDTree.m_atomic.sum);
        }
    }

    size_t approxMemoryFootprint() const {
        const size_t gridSize = m_cells.capacity() * sizeof(HashGridCell) + sizeof(*this);
        size_t totalQuadtreeSize = 0;

        for (size_t i = 0; i < m_cells.size(); i++) {
            totalQuadtreeSize += m_cells[i].quadtree.approxMemoryFootprint();
        }

        return gridSize + totalQuadtreeSize;
    }

    void build() {
        Float totalSum = 0;
        Float totalStatisticalWeight = 0;
        m_pmf.clear();

        for (const auto& cell: m_cells) {
            const Float cellSum = cell.sum.load();
            totalSum += cellSum;
            totalStatisticalWeight += cell.quadtree.statisticalWeight();
            m_pmf.append(cellSum);
        }

        for (size_t i = 0; i < m_cells.size(); i++) {
            m_cellSums[i] = m_cells[i].sum.load(std::memory_order_relaxed);
        }

        m_pmf.normalize();
        m_atomic.sum.store(totalSum, std::memory_order_relaxed);
        m_atomic.statisticalWeight.store(totalStatisticalWeight, std::memory_order_relaxed);

        for (auto& cell: m_cells) {
            cell.quadtree.build();
        }
    }

    static Vector canonical_to_world(Point2 p) {
        const Float cosTheta = 2 * p.x - 1;
        const Float phi = 2 * M_PI * p.y;

        const Float sinTheta = sqrt(1 - cosTheta * cosTheta);
        Float sinPhi, cosPhi;
        math::sincos(phi, &sinPhi, &cosPhi);

        return {sinTheta * cosPhi, sinTheta * sinPhi, cosTheta};
    }

    static Point2 worldToCanonical(const Vector& d) {
        if (!std::isfinite(d.x) || !std::isfinite(d.y) || !std::isfinite(d.z)) {
            return {0, 0};
        }

        const Float cosTheta = std::min(std::max(d.z, -1.0f), 1.0f);
        Float phi = std::atan2(d.y, d.x);
        while (phi < 0)
            phi += 2.0 * M_PI;

        return {(cosTheta + 1) / 2, phi / (2 * M_PI)};
    }


    void buildProdObjectspace(const HashGrid &dTreeSampling, Sampler *sampler,
                            const BSDF *bsdf, BSDFSamplingRecord &brec) {
        const auto bsdfLut = g_objectspaceBsdfLutMap[bsdf->getID()];
        const Point2 wiCanonical = worldToCanonical(brec.wi);
        const auto i = cellIdx(wiCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto woBsdfLut = bsdfLut[i];
        const float* cellSums = dTreeSampling.m_cellSums.data();
        m_pmf.clear();

        for (size_t k = 0; k < m_bucketCount; k++) {
            const Float radiance = cellSums[k];
            if (radiance > 0) {
                const Point2 woCanonical = worldToCanonical(brec.its.toLocal(g_cellIdxToWorldLut[k]));
                const auto k_ = cellIdx(woCanonical);
                const Float prod = radiance * woBsdfLut[k_];
                m_pmf.append(prod);
            } else {
                m_pmf.append(0);
            }
        }

        m_pmf.normalize();
    }

    void buildProdWorldspace(const HashGrid &dTreeSampling, Sampler *sampler,
                         const BSDF *bsdf, BSDFSamplingRecord &brec) {
        const auto bsdfLut = g_worldspaceBsdfLutMap[bsdf->getID()];
        const Point2 nCanonical = worldToCanonical(brec.its.geoFrame.n);
        const Point2 wiCanonical = worldToCanonical(brec.its.toWorld(brec.wi));
        const auto i = cellIdx(nCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto j = cellIdx(wiCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto woBsdfLut = bsdfLut[i][j];
        const size_t cellCount = m_cells.size();
        m_pmf.clear();

        for (size_t k = 0; k < cellCount; k++) {
            const Float radiance =
                dTreeSampling.m_cells[k].sum.load(std::memory_order_relaxed);
            if (radiance > 0) {
                const Float prod = radiance * woBsdfLut[k];
                m_pmf.append(prod);
            } else {
                m_pmf.append(0.0f);
            }
        }

        m_pmf.normalize();
    }

    // 4-wide vectorized version of prod tree building
    void buildProdWorldspace_v4(const HashGrid &dTreeSampling, Sampler *sampler,
                           const BSDF *bsdf, BSDFSamplingRecord &brec) {
        const auto bsdfLut = g_worldspaceBsdfLutMap[bsdf->getID()];
        const Point2 nCanonical = worldToCanonical(brec.its.geoFrame.n);
        const Point2 wiCanonical = worldToCanonical(brec.its.toWorld(brec.wi));
        const auto i = cellIdx(nCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto j = cellIdx(wiCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto woBsdfLut = bsdfLut[i][j];
        const size_t cellCount = m_cells.size();
        const float* cellSums = dTreeSampling.m_cellSums.data();

        m_pmf.clear();

        for (size_t kLane = 0; kLane < cellCount; kLane += 4) {
            __m128 radiance_v4 = _mm_load_ps(cellSums + kLane);
            __m128 bsdf_v4 = _mm_load_ps(woBsdfLut + kLane);
            __m128 prod_v4 = _mm_mul_ps(radiance_v4, bsdf_v4);
            float* prods = (float*)&prod_v4;
            m_pmf.append(prods[0]);
            m_pmf.append(prods[1]);
            m_pmf.append(prods[2]);
            m_pmf.append(prods[3]);
        }

        m_pmf.normalize();
    }

    // 8-wide vectorized version of prod tree building
    void buildProdWorldspace_v8(const HashGrid &dTreeSampling, Sampler *sampler,
                           const BSDF *bsdf, BSDFSamplingRecord &brec) {
        const auto bsdfLut = g_worldspaceBsdfLutMap[bsdf->getID()];
        const Point2 nCanonical = worldToCanonical(brec.its.geoFrame.n);
        const Point2 wiCanonical = worldToCanonical(brec.its.toWorld(brec.wi));
        const auto i = cellIdx(nCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto j = cellIdx(wiCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto woBsdfLut = bsdfLut[i][j];
        const float* cellSums = dTreeSampling.m_cellSums.data();

        m_pmf.clear();

        for (size_t kLane = 0; kLane < m_bucketCount; kLane += 8) {
            __m256 radiance_v8 = _mm256_loadu_ps(cellSums + kLane);
            __m256 bsdf_v8 = _mm256_loadu_ps(woBsdfLut + kLane);
            __m256 prod_v8 = _mm256_mul_ps(radiance_v8, bsdf_v8);
            float* prods = (float*)&prod_v8;
            m_pmf.append(prods[0]);
            m_pmf.append(prods[1]);
            m_pmf.append(prods[2]);
            m_pmf.append(prods[3]);
            m_pmf.append(prods[4]);
            m_pmf.append(prods[5]);
            m_pmf.append(prods[6]);
            m_pmf.append(prods[7]);
        }

        m_pmf.normalize();
    }

    void buildProdWorldspaceCompressed(const HashGrid &dTreeSampling, Sampler *sampler,
                                   const BSDF *bsdf, BSDFSamplingRecord &brec) {
        auto cellIdxTo2d = [&](const size_t cidx,
                                  const size_t gridSideLength) {
            return Point2u(cidx % gridSideLength,
                           std::floor(cidx / gridSideLength));
        };

        const auto bsdfLut = g_worldspaceBsdfLutMap[bsdf->getID()];
        const Point2 nCanonical = worldToCanonical(brec.its.geoFrame.n);
        const Point2 wiCanonical = worldToCanonical(brec.its.toWorld(brec.wi));
        const auto i = cellIdx(nCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto j = cellIdx(wiCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto ii = cellIdxTo2d(i, g_altSideLength);
        const auto jj = cellIdxTo2d(j, g_altSideLength);
        const auto h = (g_altSideLength / 2) - 1;
        const size_t width = static_cast<size_t>(m_width);
        Float* woBsdfLut_ = nullptr;
        const float* cellSums = dTreeSampling.m_cellSums.data();

        Point2u ii_;
        Point2u jj_;

        // 1st symmetry
        {
            if (ii.x > h) {
                ii_.x = g_altSideLength - 1 - ii.x;
                jj_.x = g_altSideLength - 1 - jj.x;
            } else {
                ii_.x = ii.x;
                jj_.x = jj.x;
            }
        }

        // 2nd symmetry
        {
            ii_.y = ii.y % (g_altSideLength / width);
            const int d = abs(static_cast<int>(ii_.y) - static_cast<int>(ii.y));
            const int jj_y = static_cast<int>(jj.y) - d;

            if (jj_y < 0) {
                jj_.y = jj_y + g_altSideLength;
            } else {
                jj_.y = jj_y;
            }
        }

        const auto i_ = (g_altSideLength / 2) * ii_.y + ii_.x;
        const auto j_ = g_altSideLength * jj_.y + jj_.x;
        const auto d =
            abs(static_cast<int>(ii_.y / (g_altSideLength / width)) -
                static_cast<int>(ii.y / (g_altSideLength / width)));
        woBsdfLut_ = bsdfLut[i_][j_];

        m_pmf.clear();
        const size_t cellCount = m_cells.size();

        for (size_t k = 0; k < cellCount; k++) {
            // const Float radiance = dTreeSampling.m_cells[k].sum.load(std::memory_order_relaxed);
            const Float radiance = cellSums[k];
            if (radiance > 0) {
                const auto kk = cellIdxTo2d(k, width);
                Point2u kk_;

                // 1st symmetry
                {
                    if (ii.x > h) {
                        kk_.x = width - 1 - kk.x;
                    } else {
                        kk_.x = kk.x;
                    }
                }

                // 2nd symmetry
                {
                    const int kk_y = kk.y - d;

                    if (kk_y < 0) {
                        kk_.y = kk_y + width;
                    } else {
                        kk_.y = kk_y;
                    }
                }

                const auto k_ = width * kk_.y + kk_.x;
                const Float prod = radiance * woBsdfLut_[k_];

                m_pmf.append(prod);
            } else {
                m_pmf.append(0.0f);
            }
        }

        m_pmf.normalize();
    }

    inline __m128 rshift1(__m128 v) {
        // 0b10010000
        __m128 r = _mm_permute_ps(v, 0x90);
        // 0b1110
        r = _mm_blend_ps(_mm_set1_ps(0.0f), r, 0x0e);
        return r;
    }

    inline __m128 rshift2(__m128 v) {
        // 0b01000000
        __m128 r = _mm_permute_ps(v, 0x40);
        // 0b1100
        r = _mm_blend_ps(_mm_set1_ps(0.0f), r, 0x0c);
        return r;
    }

    void buildProdWorldspaceCompressed_v4(const HashGrid &dTreeSampling, Sampler *sampler,
                                      const BSDF *bsdf, BSDFSamplingRecord &brec) {
        auto cellIdxTo2d = [&](const size_t cidx,
                                  const size_t gridSideLength) {
            return Point2u(cidx % gridSideLength,
                           std::floor(cidx / gridSideLength));
        };

        const auto bsdfLut = g_worldspaceBsdfLutMap[bsdf->getID()];
        const Point2 nCanonical = worldToCanonical(brec.its.geoFrame.n);
        const Point2 wiCanonical = worldToCanonical(brec.its.toWorld(brec.wi));
        const auto i = cellIdx(nCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto j = cellIdx(wiCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto ii = cellIdxTo2d(i, g_altSideLength);
        const auto jj = cellIdxTo2d(j, g_altSideLength);
        const auto h = (g_altSideLength / 2) - 1;
        const uint32_t width = 4;
        Float* woBsdfLut_ = nullptr;

        Point2u ii_;
        Point2u jj_;

        // 1st symmetry
        {
            if (ii.x > h) {
                ii_.x = g_altSideLength - 1 - ii.x;
                jj_.x = g_altSideLength - 1 - jj.x;
            } else {
                ii_.x = ii.x;
                jj_.x = jj.x;
            }
        }

        // 2nd symmetry
        {
            ii_.y = ii.y % (g_altSideLength / width);
            const int d = abs(static_cast<int>(ii_.y) - static_cast<int>(ii.y));
            const int jj_y = static_cast<int>(jj.y) - d;

            if (jj_y < 0) {
                jj_.y = jj_y + g_altSideLength;
            } else {
                jj_.y = jj_y;
            }
        }

        const auto i_ = (g_altSideLength / 2) * ii_.y + ii_.x;
        const auto j_ = g_altSideLength * jj_.y + jj_.x;
        const uint32_t d =
            abs(static_cast<int>(ii_.y / (g_altSideLength / width)) -
                static_cast<int>(ii.y / (g_altSideLength / width)));
        woBsdfLut_ = bsdfLut[i_][j_];

        __m128i d_v4 = _mm_set1_epi32(d);
        __m128i width_v4 = _mm_set1_epi32(width);
        __m128i widthSub1_v4 = _mm_set1_epi32(width - 1);
        const float* cellSums = dTreeSampling.m_cellSums.data();

        // m_pmf.clear();
        // m_pmf.m_cdf.resize(m_bucketCount + 1);

        __m128i kk_y_v4 = _mm_set_epi32(3, 2, 1, 0);
        kk_y_v4 = _mm_sub_epi32(kk_y_v4, d_v4);
        kk_y_v4 = _mm_add_epi32(kk_y_v4, _mm_and_si128(width_v4, _mm_cmplt_epi32(kk_y_v4, _mm_set1_epi32(0))));

        int* kk_ys = (int*)&kk_y_v4;

        __m128i kk_x_v4 = _mm_set_epi32(3, 2, 1, 0);

        if (ii.x > h) {
            kk_x_v4 = _mm_sub_epi32(widthSub1_v4, kk_x_v4);
        }

        const auto cdfPtr = m_pmf.m_cdf.data();
        Float pmfSum = 0.0f;

        for (uint32_t row = 0; row < width; row++) {
            const auto idx = row * width;
            __m128i kk_y_v4_ = _mm_set1_epi32(kk_ys[row]);
            __m128i k_v4 = _mm_add_epi32(_mm_mullo_epi32(width_v4, kk_y_v4_), kk_x_v4);

            __m128 radiance_v4 = _mm_load_ps(cellSums + idx);
            __m128 bsdf_v4 = _mm_i32gather_ps(woBsdfLut_, k_v4, 4);
            __m128 prod_v4 = _mm_mul_ps(radiance_v4, bsdf_v4);
            // float* prods = (float*)&prod_v4;
            // m_pmf.append(prods[0]);
            // m_pmf.append(prods[1]);
            // m_pmf.append(prods[2]);
            // m_pmf.append(prods[3]);

            __m128 tmp0 = rshift1(prod_v4);
            tmp0 = _mm_add_ps(prod_v4, tmp0);

            __m128 tmp1 = rshift2(tmp0);
            tmp1 = _mm_add_ps(tmp0, tmp1);
            tmp1 = _mm_add_ps(tmp1, _mm_set1_ps(pmfSum));

            _mm_storeu_ps(cdfPtr + idx + 1, tmp1);
            pmfSum = ((float*)&tmp1)[3];
        }

        m_pmf.normalize();

        // Vectorized PMF normalization
        // m_pmf.m_sum = pmfSum;
        // if (pmfSum > 0) {
        //     // const auto cdfPtr = m_pmf.m_cdf.data();
        //     m_pmf.m_normalization = 1.0f / pmfSum;
        //     __m256 norm_v8 = _mm256_set1_ps(m_pmf.m_normalization);
        //     for (size_t i = 1; i < m_bucketCount + 1; i += 8) {
        //         __m256 cdf_v8 = _mm256_loadu_ps(cdfPtr + i);
        //         cdf_v8 = _mm256_mul_ps(cdf_v8, norm_v8);
        //         _mm256_storeu_ps(cdfPtr + i, cdf_v8);
        //     }
        //     cdfPtr[m_bucketCount] = 1.0f;
        //     m_pmf.m_normalized = true;
        // } else {
        //     m_pmf.m_normalization = 0.0f;
        // }
    }

    inline __m256 rshift1(__m256 v) {
        __m256i ctl = _mm256_set_epi32(6, 5, 4, 3, 2, 1, 0, 0);
        __m256 r = _mm256_permutevar8x32_ps(v, ctl);
        r = _mm256_blend_ps(_mm256_set1_ps(0.0f), r, 0xfe);
        return r;
    }

    inline __m256 rshift2(__m256 v) {
        __m256i ctl = _mm256_set_epi32(5, 4, 3, 2, 1, 0, 0, 0);
        __m256 r = _mm256_permutevar8x32_ps(v, ctl);
        r = _mm256_blend_ps(_mm256_set1_ps(0.0f), r, 0xfc);
        return r;
    }

    inline __m256 rshift4(__m256 v) {
        __m256i ctl = _mm256_set_epi32(3, 2, 1, 0, 0, 0, 0, 0);
        __m256 r = _mm256_permutevar8x32_ps(v, ctl);
        r = _mm256_blend_ps(_mm256_set1_ps(0.0f), r, 0xf0);
        return r;
    }

    void buildProdWorldspaceCompressed_v8(const HashGrid &dTreeSampling, Sampler *sampler,
                                      const BSDF *bsdf, BSDFSamplingRecord &brec) {
        auto cellIdxTo2d = [&](const size_t cidx,
                                  const size_t gridSideLength) {
            return Point2u(cidx % gridSideLength,
                           std::floor(cidx / gridSideLength));
        };

        const auto bsdfLut = g_worldspaceBsdfLutMap[bsdf->getID()];
        const Point2 nCanonical = worldToCanonical(brec.its.geoFrame.n);
        const Point2 wiCanonical = worldToCanonical(brec.its.toWorld(brec.wi));
        const auto i = cellIdx(nCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto j = cellIdx(wiCanonical, g_altSideLength, g_altCellSize, g_altCellCount);
        const auto ii = cellIdxTo2d(i, g_altSideLength);
        const auto jj = cellIdxTo2d(j, g_altSideLength);
        const auto h = (g_altSideLength / 2) - 1;
        const uint32_t width = 8;
        Float* woBsdfLut_ = nullptr;

        Point2u ii_;
        Point2u jj_;

        // 1st symmetry
        {
            if (ii.x > h) {
                ii_.x = g_altSideLength - 1 - ii.x;
                jj_.x = g_altSideLength - 1 - jj.x;
            } else {
                ii_.x = ii.x;
                jj_.x = jj.x;
            }
        }

        // 2nd symmetry
        {
            ii_.y = ii.y % (g_altSideLength / width);
            const int d = abs(static_cast<int>(ii_.y) - static_cast<int>(ii.y));
            const int jj_y = static_cast<int>(jj.y) - d;

            if (jj_y < 0) {
                jj_.y = jj_y + g_altSideLength;
            } else {
                jj_.y = jj_y;
            }
        }

        const auto i_ = (g_altSideLength / 2) * ii_.y + ii_.x;
        const auto j_ = g_altSideLength * jj_.y + jj_.x;
        const uint32_t d =
            abs(static_cast<int>(ii_.y / (g_altSideLength / width)) -
                static_cast<int>(ii.y / (g_altSideLength / width)));
        woBsdfLut_ = bsdfLut[i_][j_];

        __m256i d_v8 = _mm256_set1_epi32(d);
        __m256i width_v8 = _mm256_set1_epi32(width);
        __m256i widthSub1_v8 = _mm256_set1_epi32(width - 1);
        const float* cellSums = dTreeSampling.m_cellSums.data();

        __m256i kk_y_v8 = _mm256_set_epi32(7, 6, 5, 4, 3, 2, 1, 0);
        kk_y_v8 = _mm256_sub_epi32(kk_y_v8, d_v8);
        kk_y_v8 = _mm256_add_epi32(kk_y_v8, _mm256_and_si256(width_v8, _mm256_cmpgt_epi32(_mm256_set1_epi32(0), kk_y_v8)));

        int* kk_ys = (int*)&kk_y_v8;

        __m256i kk_x_v8 = _mm256_set_epi32(7, 6, 5, 4, 3, 2, 1, 0);

        if (ii.x > h) {
            kk_x_v8 = _mm256_sub_epi32(widthSub1_v8, kk_x_v8);
        }

        // m_pmf.m_cdf.resize(m_bucketCount + 1);
        // m_pmf.m_cdf[0] = 0.0f;
        // m_pmf.clear();

        const auto cdfPtr = m_pmf.m_cdf.data();
        Float pmfSum = 0.0f;

        for (uint32_t row = 0; row < width; row++) {
            const auto idx = row * width;
            __m256i kk_y_v8_ = _mm256_set1_epi32(kk_ys[row]);
            __m256i k_v8 = _mm256_add_epi32(_mm256_mullo_epi32(width_v8, kk_y_v8_), kk_x_v8);

            __m256 radiance_v8 = _mm256_loadu_ps(cellSums + idx);
            __m256 bsdf_v8 = _mm256_i32gather_ps(woBsdfLut_, k_v8, 4);
            __m256 prod_v8 = _mm256_mul_ps(radiance_v8, bsdf_v8);

            __m256 tmp0 = rshift1(prod_v8);
            tmp0 = _mm256_add_ps(prod_v8, tmp0);

            __m256 tmp1 = rshift2(tmp0);
            tmp1 = _mm256_add_ps(tmp0, tmp1);

            __m256 tmp2 = rshift4(tmp1);
            tmp2 = _mm256_add_ps(tmp1, tmp2);
            tmp2 = _mm256_add_ps(tmp2, _mm256_set1_ps(pmfSum));
            _mm256_storeu_ps(cdfPtr + idx + 1, tmp2);
            pmfSum = ((float*)&tmp2)[7];
        }

        m_pmf.normalize();

        // Vectorized PMF normalization
        // m_pmf.m_sum = pmfSum;
        // if (pmfSum > 0) {
        //     // const auto cdfPtr = m_pmf.m_cdf.data();
        //     m_pmf.m_normalization = 1.0f / pmfSum;
        //     __m256 norm_v8 = _mm256_set1_ps(m_pmf.m_normalization);
        //     for (size_t i = 1; i < m_bucketCount + 1; i += 8) {
        //         __m256 cdf_v8 = _mm256_loadu_ps(cdfPtr + i);
        //         cdf_v8 = _mm256_mul_ps(cdf_v8, norm_v8);
        //         _mm256_storeu_ps(cdfPtr + i, cdf_v8);
        //     }
        //     cdfPtr[m_bucketCount] = 1.0f;
        //     m_pmf.m_normalized = true;
        // } else {
        //     m_pmf.m_normalization = 0.0f;
        // }
    }

    void buildProdRobust(const HashGrid& dTreeSampling, Sampler* sampler, const BSDF* bsdf, BSDFSamplingRecord& brec) {
        const Float* cellSums = dTreeSampling.m_cellSums.data();
        m_pmf.clear();

        for (size_t k = 0; k < m_bucketCount; k++) {
            const Float radiance = cellSums[k];

            if (radiance > 0) {
                brec.wo = brec.its.toLocal(g_cellIdxToWorldLut[k]);
                const Float prod = radiance * bsdf->eval(brec).average();
                m_pmf.append(prod);
            } else {
                m_pmf.append(0.0f);
            }
        }

        m_pmf.normalize();
    }

private:

    struct Atomic {
        Atomic() {
            sum.store(0, std::memory_order_relaxed);
            statisticalWeight.store(0, std::memory_order_relaxed);
        }

        Atomic(const Atomic& arg) {
            *this = arg;
        }

        Atomic& operator=(const Atomic& arg) {
            sum.store(arg.sum.load(std::memory_order_relaxed), std::memory_order_relaxed);
            statisticalWeight.store(arg.statisticalWeight.load(std::memory_order_relaxed), std::memory_order_relaxed);
            return *this;
        }

        std::atomic<Float> sum;
        std::atomic<Float> statisticalWeight;

    } m_atomic;

    Float m_cellSize;
    Float m_minExtent;
    Float m_maxExtent;
    Float m_width;
    size_t m_bucketCount;

    DiscreteDistribution m_pmf;
    std::vector<HashGridCell> m_cells;
    std::vector<Float> m_cellSums;
};

struct DTreeRecord {
    Vector d;
    Float radiance, product;
    Float woPdf, bsdfPdf, dTreePdf;
    Float statisticalWeight;
    bool isDelta;
};

struct DTreeWrapper {
public:
    DTreeWrapper() {
        building = HashGrid(g_defaultCellSize, 0.0f, 1.0f);
        sampling = HashGrid(g_defaultCellSize, 0.0f, 1.0f);
    }

    void record(const DTreeRecord& rec, EDirectionalFilter directionalFilter, EBsdfSamplingFractionLoss bsdfSamplingFractionLoss) {
        if (!rec.isDelta) {
            Float irradiance = rec.radiance / rec.woPdf;
            building.recordIrradiance(dirToCanonical(rec.d), irradiance, rec.statisticalWeight, directionalFilter);
        }

        if (bsdfSamplingFractionLoss != EBsdfSamplingFractionLoss::ENone && rec.product > 0) {
            optimizeBsdfSamplingFraction(rec, bsdfSamplingFractionLoss == EBsdfSamplingFractionLoss::EKL ? 1.0f : 2.0f);
        }
    }

    static Vector canonicalToDir(Point2 p) {
        const Float cosTheta = 2 * p.x - 1;
        const Float phi = 2 * M_PI * p.y;

        const Float sinTheta = sqrt(1 - cosTheta * cosTheta);
        Float sinPhi, cosPhi;
        math::sincos(phi, &sinPhi, &cosPhi);

        return {sinTheta * cosPhi, sinTheta * sinPhi, cosTheta};
    }

    static Point2 dirToCanonical(const Vector& d) {
        if (!std::isfinite(d.x) || !std::isfinite(d.y) || !std::isfinite(d.z)) {
            return {0, 0};
        }

        const Float cosTheta = std::min(std::max(d.z, -1.0f), 1.0f);
        Float phi = std::atan2(d.y, d.x);
        while (phi < 0)
            phi += 2.0 * M_PI;

        return {(cosTheta + 1) / 2, phi / (2 * M_PI)};
    }

    void build() {
        building.build();
        sampling = building;
    }

    void reset(int maxDepth, Float subdivisionThreshold) {
        building.reset(sampling, maxDepth, subdivisionThreshold);
    }

    Vector sample(Sampler* sampler) const {
        return canonicalToDir(sampling.sample(sampler));
    }

    static Vector sample(Sampler* sampler, const DTreeWrapper* gridWrapper, const DTreeWrapper* quadtreeWrapper) {
        // NOTE: The grid pmf normalization factor may become 'inf'.
        // if (!(gridWrapper->sampling.mean() > 0) || !std::isfinite(gridWrapper->sampling.pmfNorm())) {
        //     return canonicalToDir(sampler->next2D());
        // }

        if (!(gridWrapper->sampling.pmfSum() > 0) || !std::isfinite(gridWrapper->sampling.pmfNorm())) {
            return canonicalToDir(sampler->next2D());
        }

        const size_t cidx = gridWrapper->sampling.sample_grid(sampler);
        const Point2 quadtreeOrigin = gridWrapper->sampling.cellIdxToOrigin(cidx);
        const Float quadtreeScale = gridWrapper->sampling.cellSize();
        const Point2 dirCanonical = quadtreeOrigin + quadtreeScale * quadtreeWrapper->sampling.sample_quadtree(sampler, cidx);
        return canonicalToDir(dirCanonical);
    }

    Float pdf(const Vector& dir) const {
        return sampling.pdf(dirToCanonical(dir));
    }

    static Float pdf(const Vector& dir, const DTreeWrapper* gridWrapper, const DTreeWrapper* quadtreeWrapper) {
        // NOTE: The grid pmf normalization factor may become 'inf'.
        // if (!(gridWrapper->sampling.mean() > 0) || !std::isfinite(gridWrapper->sampling.pmfNorm())) {
        //     return 1 / (4 * M_PI);
        // }

        if (!(gridWrapper->sampling.pmfSum() > 0) || !std::isfinite(gridWrapper->sampling.pmfNorm())) {
            return 1 / (4 * M_PI);
        }

        const auto pGrid = dirToCanonical(dir);
        const size_t cidx = gridWrapper->sampling.cellIdx(pGrid);
        const Point2 quadtreeOrigin = gridWrapper->sampling.cellIdxToOrigin(cidx);
        const Float quadtreeScale = gridWrapper->sampling.cellSize();
        const Point2 pQuadtree = (pGrid + -quadtreeOrigin) / quadtreeScale;

        const auto pdfGrid = gridWrapper->sampling.pdfGrid(pGrid);
        const auto pdfQuadtree = quadtreeWrapper->sampling.pdfQuadtree(pGrid, pQuadtree);
        return pdfGrid * pdfQuadtree;
    }

    Float diff(const DTreeWrapper& other) const {
        return 0.0f;
    }

    int depth() const {
        return sampling.depth();
    }

    size_t numNodes() const {
        return sampling.numNodes();
    }

    Float meanRadiance() const {
        return sampling.mean();
    }

    Float statisticalWeight() const {
        return sampling.statisticalWeight();
    }

    Float statisticalWeightBuilding() const {
        return building.statisticalWeight();
    }

    void setStatisticalWeightBuilding(Float statisticalWeight) {
        building.setStatisticalWeight(statisticalWeight);
    }

    void div2AllStatisticalWeightsBuilding() {
        building.div2AllStatisticalWeights();
    }

    size_t approxMemoryFootprint() const {
        return building.approxMemoryFootprint() + sampling.approxMemoryFootprint();
    }

    inline Float bsdfSamplingFraction(Float variable) const {
        return logistic(variable);
    }

    inline Float dBsdfSamplingFraction_dVariable(Float variable) const {
        Float fraction = bsdfSamplingFraction(variable);
        return fraction * (1 - fraction);
    }

    inline Float bsdfSamplingFraction() const {
        return bsdfSamplingFraction(bsdfSamplingFractionOptimizer.variable());
    }

    void optimizeBsdfSamplingFraction(const DTreeRecord& rec, Float ratioPower) {
        m_lock.lock();

        // GRADIENT COMPUTATION
        Float variable = bsdfSamplingFractionOptimizer.variable();
        Float samplingFraction = bsdfSamplingFraction(variable);

        // Loss gradient w.r.t. sampling fraction
        Float mixPdf = samplingFraction * rec.bsdfPdf + (1 - samplingFraction) * rec.dTreePdf;
        Float ratio = std::pow(rec.product / mixPdf, ratioPower);
        Float dLoss_dSamplingFraction = -ratio / rec.woPdf * (rec.bsdfPdf - rec.dTreePdf);

        // Chain rule to get loss gradient w.r.t. trainable variable
        Float dLoss_dVariable = dLoss_dSamplingFraction * dBsdfSamplingFraction_dVariable(variable);

        // We want some regularization such that our parameter does not become too big.
        // We use l2 regularization, resulting in the following linear gradient.
        Float l2RegGradient = 0.01f * variable;

        Float lossGradient = l2RegGradient + dLoss_dVariable;

        // ADAM GRADIENT DESCENT
        bsdfSamplingFractionOptimizer.append(lossGradient, rec.statisticalWeight);

        m_lock.unlock();
    }

    void dump(BlobWriter& blob, const Point& p, const Vector& size) const {
        // Not implemented
    }

    void build_prod(DTreeWrapper* prodDTree, Sampler* sampler, const BSDF* bsdf, BSDFSamplingRecord& brec) {
        switch(g_bsdfLutType) {
        case EBsdfLutType::ENone: {
            prodDTree->sampling.buildProdRobust(sampling, sampler, bsdf, brec);
            break;
        }
        case EBsdfLutType::EObjectSpace: {
            prodDTree->sampling.buildProdObjectspace(sampling, sampler, bsdf, brec);
            break;
        }
        case EBsdfLutType::EWorldSpace: {
            if (g_enableWorldspaceLutCompression) {
                if (g_enableSimd) {
                    switch(g_defaultGridSideLength) {
                    case 4:
                        prodDTree->sampling.buildProdWorldspaceCompressed_v4(sampling, sampler, bsdf, brec);
                        break;
                    case 8:
                        prodDTree->sampling.buildProdWorldspaceCompressed_v8(sampling, sampler, bsdf, brec);
                        break;
                    default:
                        prodDTree->sampling.buildProdWorldspaceCompressed(sampling, sampler, bsdf, brec);
                    }
                } else {
                    prodDTree->sampling.buildProdWorldspaceCompressed(sampling, sampler, bsdf, brec);
                }
            } else {
                if (g_enableSimd) {
                    prodDTree->sampling.buildProdWorldspace_v8(sampling, sampler, bsdf, brec);
                } else {
                    prodDTree->sampling.buildProdWorldspace(sampling, sampler, bsdf, brec);
                }
            }

            break;
        }
        }
    }

private:
    HashGrid building;
    HashGrid sampling;

    AdamOptimizer bsdfSamplingFractionOptimizer{0.01f};

    class SpinLock {
    public:
        SpinLock() {
            m_mutex.clear(std::memory_order_release);
        }

        SpinLock(const SpinLock& other) { m_mutex.clear(std::memory_order_release); }
        SpinLock& operator=(const SpinLock& other) { return *this; }

        void lock() {
            while (m_mutex.test_and_set(std::memory_order_acquire)) { }
        }

        void unlock() {
            m_mutex.clear(std::memory_order_release);
        }
    private:
        std::atomic_flag m_mutex;
    } m_lock;
};

struct STreeNode {
    STreeNode() {
        children = {};
        isLeaf = true;
        axis = 0;
    }

    int childIndex(Point& p) const {
        if (p[axis] < 0.5f) {
            p[axis] *= 2;
            return 0;
        } else {
            p[axis] = (p[axis] - 0.5f) * 2;
            return 1;
        }
    }

    int nodeIndex(Point& p) const {
        return children[childIndex(p)];
    }

    DTreeWrapper* dTreeWrapper(Point& p, Vector& size, std::vector<STreeNode>& nodes) {
        SAssert(p[axis] >= 0 && p[axis] <= 1);
        if (isLeaf) {
            return &dTree;
        } else {
            size[axis] /= 2;
            return nodes[nodeIndex(p)].dTreeWrapper(p, size, nodes);
        }
    }

    const DTreeWrapper* dTreeWrapper() const {
        return &dTree;
    }

    int depth(Point& p, const std::vector<STreeNode>& nodes) const {
        SAssert(p[axis] >= 0 && p[axis] <= 1);
        if (isLeaf) {
            return 1;
        } else {
            return 1 + nodes[nodeIndex(p)].depth(p, nodes);
        }
    }

    int depth(const std::vector<STreeNode>& nodes) const {
        int result = 1;

        if (!isLeaf) {
            for (auto c : children) {
                result = std::max(result, 1 + nodes[c].depth(nodes));
            }
        }

        return result;
    }

    void forEachLeaf(
        std::function<void(const DTreeWrapper*, const Point&, const Vector&)> func,
        Point p, Vector size, const std::vector<STreeNode>& nodes) const {

        if (isLeaf) {
            func(&dTree, p, size);
        } else {
            size[axis] /= 2;
            for (int i = 0; i < 2; ++i) {
                Point childP = p;
                if (i == 1) {
                    childP[axis] += size[axis];
                }

                nodes[children[i]].forEachLeaf(func, childP, size, nodes);
            }
        }
    }

    Float computeOverlappingVolume(const Point& min1, const Point& max1, const Point& min2, const Point& max2) {
        Float lengths[3];
        for (int i = 0; i < 3; ++i) {
            lengths[i] = std::max(std::min(max1[i], max2[i]) - std::max(min1[i], min2[i]), 0.0f);
        }
        return lengths[0] * lengths[1] * lengths[2];
    }

    void record(const Point& min1, const Point& max1, Point min2, Vector size2, const DTreeRecord& rec, EDirectionalFilter directionalFilter, EBsdfSamplingFractionLoss bsdfSamplingFractionLoss, std::vector<STreeNode>& nodes) {
        Float w = computeOverlappingVolume(min1, max1, min2, min2 + size2);
        if (w > 0) {
            if (isLeaf) {
                dTree.record({ rec.d, rec.radiance, rec.product, rec.woPdf, rec.bsdfPdf, rec.dTreePdf, rec.statisticalWeight * w, rec.isDelta }, directionalFilter, bsdfSamplingFractionLoss);
            } else {
                size2[axis] /= 2;
                for (int i = 0; i < 2; ++i) {
                    if (i & 1) {
                        min2[axis] += size2[axis];
                    }

                    nodes[children[i]].record(min1, max1, min2, size2, rec, directionalFilter, bsdfSamplingFractionLoss, nodes);
                }
            }
        }
    }

    bool isLeaf;
    DTreeWrapper dTree;
    int axis;
    std::array<uint32_t, 2> children;
};

class STree {
public:
    STree(const AABB& aabb) {
        clear();

        m_aabb = aabb;

        // Enlarge AABB to turn it into a cube. This has the effect
        // of nicer hierarchical subdivisions.
        Vector size = m_aabb.max - m_aabb.min;
        Float maxSize = std::max(std::max(size.x, size.y), size.z);
        m_aabb.max = m_aabb.min + Vector(maxSize);
    }

    void clear() {
        m_nodes.clear();
        m_nodes.emplace_back();
    }

    void subdivideAll() {
        int nNodes = (int)m_nodes.size();
        for (int i = 0; i < nNodes; ++i) {
            if (m_nodes[i].isLeaf) {
                subdivide(i, m_nodes);
            }
        }
    }

    void subdivide(int nodeIdx, std::vector<STreeNode>& nodes) {
        // Add 2 child nodes
        nodes.resize(nodes.size() + 2);

        if (nodes.size() > std::numeric_limits<uint32_t>::max()) {
            SLog(EWarn, "DTreeWrapper hit maximum children count.");
            return;
        }

        STreeNode& cur = nodes[nodeIdx];
        for (int i = 0; i < 2; ++i) {
            uint32_t idx = (uint32_t)nodes.size() - 2 + i;
            cur.children[i] = idx;
            nodes[idx].axis = (cur.axis + 1) % 3;
            nodes[idx].dTree = cur.dTree;
            nodes[idx].dTree.div2AllStatisticalWeightsBuilding();
        }
        cur.isLeaf = false;
        cur.dTree = {}; // Reset to an empty dtree to save memory.
    }

    DTreeWrapper* dTreeWrapper(Point p, Vector& size) {
        size = m_aabb.getExtents();
        p = Point(p - m_aabb.min);
        p.x /= size.x;
        p.y /= size.y;
        p.z /= size.z;

        return m_nodes[0].dTreeWrapper(p, size, m_nodes);
    }

    DTreeWrapper* dTreeWrapper(Point p) {
        Vector size;
        return dTreeWrapper(p, size);
    }

    void forEachDTreeWrapperConst(std::function<void(const DTreeWrapper*)> func) const {
        for (auto& node : m_nodes) {
            if (node.isLeaf) {
                func(&node.dTree);
            }
        }
    }

    void forEachDTreeWrapperConstP(std::function<void(const DTreeWrapper*, const Point&, const Vector&)> func) const {
        m_nodes[0].forEachLeaf(func, m_aabb.min, m_aabb.max - m_aabb.min, m_nodes);
    }

    void forEachDTreeWrapperParallel(std::function<void(DTreeWrapper*)> func) {
        int nDTreeWrappers = static_cast<int>(m_nodes.size());

#pragma omp parallel for
        for (int i = 0; i < nDTreeWrappers; ++i) {
            if (m_nodes[i].isLeaf) {
                func(&m_nodes[i].dTree);
            }
        }
    }

    void record(const Point& p, const Vector& dTreeVoxelSize, DTreeRecord rec, EDirectionalFilter directionalFilter, EBsdfSamplingFractionLoss bsdfSamplingFractionLoss) {
        Float volume = 1;
        for (int i = 0; i < 3; ++i) {
            volume *= dTreeVoxelSize[i];
        }

        rec.statisticalWeight /= volume;
        m_nodes[0].record(p - dTreeVoxelSize * 0.5f, p + dTreeVoxelSize * 0.5f, m_aabb.min, m_aabb.getExtents(), rec, directionalFilter, bsdfSamplingFractionLoss, m_nodes);
    }

    void dump(BlobWriter& blob) const {
        forEachDTreeWrapperConstP([&blob](const DTreeWrapper* dTree, const Point& p, const Vector& size) {
            if (dTree->statisticalWeight() > 0) {
                dTree->dump(blob, p, size);
            }
        });
    }

    size_t approxMemoryFootprint()
    {
        size_t totalMemFootprint = 0;
        for (const auto& node : m_nodes)
        {
            totalMemFootprint += node.dTreeWrapper()->approxMemoryFootprint();
        }

        return totalMemFootprint;
    }

    bool shallSplit(const STreeNode& node, int depth, size_t samplesRequired) {
        return m_nodes.size() < std::numeric_limits<uint32_t>::max() - 1 && node.dTree.statisticalWeightBuilding() > samplesRequired;
    }

    void refine(size_t sTreeThreshold, int maxMB) {
        if (maxMB >= 0) {
            size_t approxMemoryFootprint = 0;
            for (const auto& node : m_nodes) {
                approxMemoryFootprint += node.dTreeWrapper()->approxMemoryFootprint();
            }

            if (approxMemoryFootprint / 1000000 >= (size_t)maxMB) {
                return;
            }
        }

        struct StackNode {
            size_t index;
            int depth;
        };

        std::stack<StackNode> nodeIndices;
        nodeIndices.push({0,  1});
        while (!nodeIndices.empty()) {
            StackNode sNode = nodeIndices.top();
            nodeIndices.pop();

            // Subdivide if needed and leaf
            if (m_nodes[sNode.index].isLeaf) {
                if (shallSplit(m_nodes[sNode.index], sNode.depth, sTreeThreshold)) {
                    subdivide((int)sNode.index, m_nodes);
                }
            }

            // Add children to stack if we're not
            if (!m_nodes[sNode.index].isLeaf) {
                const STreeNode& node = m_nodes[sNode.index];
                for (int i = 0; i < 2; ++i) {
                    nodeIndices.push({node.children[i], sNode.depth + 1});
                }
            }
        }

        // Uncomment once memory becomes an issue.
        //m_nodes.shrink_to_fit();
    }

    const AABB& aabb() const {
        return m_aabb;
    }

private:
    std::vector<STreeNode> m_nodes;
    AABB m_aabb;
};

static StatsCounter avgPathLength("Hybrid Guided path tracer", "Average path length", EAverage);
static StatsCounter percGuiding("Hybrid Guided path tracer",
                                "Percentage of guiding type (BSDF or DTree)",
                                EPercentage);
static StatsCounter percNonzero("Hybrid Guided path tracer", "Nonzero path percentage", EPercentage);
static StatsCounter avgDtBuildProd("Hybrid Guided path tracer", "[BuildProd] Average runtime (ns)", EAverage);

class HybridGuidedPathTracer : public MonteCarloIntegrator {
public:
    HybridGuidedPathTracer(const Properties &props) : MonteCarloIntegrator(props) {
        m_neeStr = props.getString("nee", "never");
        if (m_neeStr == "never") {
            m_nee = ENever;
        } else if (m_neeStr == "kickstart") {
            m_nee = EKickstart;
        } else if (m_neeStr == "always") {
            m_nee = EAlways;
        } else {
            Assert(false);
        }

        m_sampleCombinationStr = props.getString("sampleCombination", "automatic");
        if (m_sampleCombinationStr == "discard") {
            m_sampleCombination = ESampleCombination::EDiscard;
        } else if (m_sampleCombinationStr == "automatic") {
            m_sampleCombination = ESampleCombination::EDiscardWithAutomaticBudget;
        } else if (m_sampleCombinationStr == "inversevar") {
            m_sampleCombination = ESampleCombination::EInverseVariance;
        } else {
            Assert(false);
        }

        m_spatialFilterStr = props.getString("spatialFilter", "nearest");
        if (m_spatialFilterStr == "nearest") {
            m_spatialFilter = ESpatialFilter::ENearest;
        } else if (m_spatialFilterStr == "stochastic") {
            m_spatialFilter = ESpatialFilter::EStochasticBox;
        } else if (m_spatialFilterStr == "box") {
            m_spatialFilter = ESpatialFilter::EBox;
        } else {
            Assert(false);
        }

        m_directionalFilterStr = props.getString("directionalFilter", "nearest");
        if (m_directionalFilterStr == "nearest") {
            m_directionalFilter = EDirectionalFilter::ENearest;
        } else if (m_directionalFilterStr == "box") {
            m_directionalFilter = EDirectionalFilter::EBox;
        } else {
            Assert(false);
        }

        m_bsdfSamplingFractionLossStr = props.getString("bsdfSamplingFractionLoss", "none");
        if (m_bsdfSamplingFractionLossStr == "none") {
            m_bsdfSamplingFractionLoss = EBsdfSamplingFractionLoss::ENone;
        } else if (m_bsdfSamplingFractionLossStr == "kl") {
            m_bsdfSamplingFractionLoss = EBsdfSamplingFractionLoss::EKL;
        } else if (m_bsdfSamplingFractionLossStr == "var") {
            m_bsdfSamplingFractionLoss = EBsdfSamplingFractionLoss::EVariance;
        } else {
            Assert(false);
        }

        m_sdTreeMaxMemory = props.getInteger("sdTreeMaxMemory", -1);
        m_sTreeThreshold = props.getInteger("sTreeThreshold", 12000);
        m_dTreeThreshold = props.getFloat("dTreeThreshold", 0.01f);
        m_bsdfSamplingFraction = props.getFloat("bsdfSamplingFraction", 0.5f);
        m_sppPerPass = props.getInteger("sppPerPass", 4);

        m_budgetStr = props.getString("budgetType", "seconds");
        if (m_budgetStr == "spp") {
            m_budgetType = ESpp;
        } else if (m_budgetStr == "seconds") {
            m_budgetType = ESeconds;
        } else {
            Assert(false);
        }

        m_budget = props.getFloat("budget", 300.0f);
        m_dumpSDTree = props.getBoolean("dumpSDTree", false);

        /* Product path guiding settings */
        m_enableProductIs = props.getBoolean("enableProductIs", true);
        m_gridSideLength = props.getInteger("gridSideLength", 10);
        g_defaultGridSideLength = m_gridSideLength;
        g_defaultCellSize = 1.0f / static_cast<Float>(m_gridSideLength);

        const std::string bsdfLutTypeStr = props.getString("bsdfLutType", "worldspace");
        if (bsdfLutTypeStr == "none") {
            g_bsdfLutType = EBsdfLutType::ENone;
        } else if (bsdfLutTypeStr == "objectspace") {
            g_bsdfLutType = EBsdfLutType::EObjectSpace;
        } else if (bsdfLutTypeStr == "worldspace") {
            g_bsdfLutType = EBsdfLutType::EWorldSpace;
        } else {
            Assert(false);
        }

        g_enableWorldspaceLutCompression = props.getBoolean("enableWorldspaceLutCompression", false);
        g_bsdfLutSampleCount = props.getInteger("bsdfLutSampleCount", 4);
        g_enableSimd = props.getBoolean("enableSimd", false);
    }

    ref<BlockedRenderProcess> renderPass(Scene *scene,
        RenderQueue *queue, const RenderJob *job,
        int sceneResID, int sensorResID, int samplerResID, int integratorResID) {

        /* This is a sampling-based integrator - parallelize */
        ref<BlockedRenderProcess> proc = new BlockedRenderProcess(job,
            queue, scene->getBlockSize());

        proc->disableProgress();

        proc->bindResource("integrator", integratorResID);
        proc->bindResource("scene", sceneResID);
        proc->bindResource("sensor", sensorResID);
        proc->bindResource("sampler", samplerResID);

        scene->bindUsedResources(proc);
        bindUsedResources(proc);

        return proc;
    }

    void resetSDTree() {
        Log(EInfo, "Resetting distributions for sampling.");

        m_sdTree->refine((size_t)(std::sqrt(std::pow(2, m_iter) * m_sppPerPass / 4) * m_sTreeThreshold), m_sdTreeMaxMemory);
        m_sdTree->forEachDTreeWrapperParallel([this](DTreeWrapper* dTree) { dTree->reset(20, m_dTreeThreshold); });
    }

    void buildSDTree(const Scene* scene) {
        Log(EInfo, "Building distributions for sampling.");

        // Build distributions
        m_sdTree->forEachDTreeWrapperParallel([](DTreeWrapper* dTree) { dTree->build(); });

        // Gather statistics
        int maxDepth = 0;
        int minDepth = std::numeric_limits<int>::max();
        Float avgDepth = 0;
        Float maxAvgRadiance = 0;
        Float minAvgRadiance = std::numeric_limits<Float>::max();
        Float avgAvgRadiance = 0;
        size_t totalNodes = 0;
        size_t maxNodes = 0;
        size_t minNodes = std::numeric_limits<size_t>::max();
        Float avgNodes = 0;
        Float maxStatisticalWeight = 0;
        Float minStatisticalWeight = std::numeric_limits<Float>::max();
        Float avgStatisticalWeight = 0;

        int nPoints = 0;
        int nPointsNodes = 0;

        m_sdTree->forEachDTreeWrapperConst([&](const DTreeWrapper* dTree) {
            const int depth = dTree->depth();
            maxDepth = std::max(maxDepth, depth);
            minDepth = std::min(minDepth, depth);
            avgDepth += depth;

            const Float avgRadiance = dTree->meanRadiance();
            maxAvgRadiance = std::max(maxAvgRadiance, avgRadiance);
            minAvgRadiance = std::min(minAvgRadiance, avgRadiance);
            avgAvgRadiance += avgRadiance;

            if (dTree->numNodes() > 1) {
                const size_t nodes = dTree->numNodes();
                maxNodes = std::max(maxNodes, nodes);
                minNodes = std::min(minNodes, nodes);
                avgNodes += nodes;
                totalNodes += nodes;
                ++nPointsNodes;
            }

            const Float statisticalWeight = dTree->statisticalWeight();
            maxStatisticalWeight = std::max(maxStatisticalWeight, statisticalWeight);
            minStatisticalWeight = std::min(minStatisticalWeight, statisticalWeight);
            avgStatisticalWeight += statisticalWeight;

            ++nPoints;
        });

        if (nPoints > 0) {
            avgDepth /= nPoints;
            avgAvgRadiance /= nPoints;

            if (nPointsNodes > 0) {
                avgNodes /= nPointsNodes;
            }

            avgStatisticalWeight /= nPoints;
        }

        Log(EInfo,
            "Distribution statistics:\n"
            "  Depth         = [%d, %f, %d]\n"
            "  Mean radiance = [%f, %f, %f]\n"
            "  Node count    = [" SIZE_T_FMT ", %f, " SIZE_T_FMT ", " SIZE_T_FMT "]\n"
            "  Stat. weight  = [%f, %f, %f]\n",
            minDepth, avgDepth, maxDepth,
            minAvgRadiance, avgAvgRadiance, maxAvgRadiance,
            minNodes, avgNodes, maxNodes, totalNodes,
            minStatisticalWeight, avgStatisticalWeight, maxStatisticalWeight
        );

        m_isBuilt = true;
    }

    void dumpSDTree(Scene* scene, ref<Sensor> sensor) {
        std::ostringstream extension;
        extension << "-" << std::setfill('0') << std::setw(2) << m_iter << ".sdt";
        fs::path path = scene->getDestinationFile();
        path = path.parent_path() / (path.leaf().string() + extension.str());

        auto cameraMatrix = sensor->getWorldTransform()->eval(0).getMatrix();

        BlobWriter blob(path.string());

        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                blob << (float)cameraMatrix(i, j);
            }
        }

        m_sdTree->dump(blob);
    }

    bool performRenderPasses(Float& variance, int numPasses, Scene *scene, RenderQueue *queue, const RenderJob *job,
        int sceneResID, int sensorResID, int samplerResID, int integratorResID) {

        ref<Scheduler> sched = Scheduler::getInstance();
        ref<Sensor> sensor = static_cast<Sensor *>(sched->getResource(sensorResID));
        ref<Film> film = sensor->getFilm();

        m_image->clear();
        m_squaredImage->clear();

        size_t totalBlocks = 0;

        Log(EInfo, "Rendering %d render passes.", numPasses);

        auto start = std::chrono::steady_clock::now();

        for (int i = 0; i < numPasses; ++i) {
            ref<BlockedRenderProcess> process = renderPass(scene, queue, job, sceneResID, sensorResID, samplerResID, integratorResID);
            m_renderProcesses.push_back(process);
            totalBlocks += process->totalBlocks();
        }

        bool result = true;
        int passesRenderedLocal = 0;

        static const size_t processBatchSize = 128;

        for (size_t i = 0; i < m_renderProcesses.size(); i += processBatchSize) {
            const size_t start = i;
            const size_t end = std::min(i + processBatchSize, m_renderProcesses.size());
            for (size_t j = start; j < end; ++j) {
                sched->schedule(m_renderProcesses[j]);
            }

            for (size_t j = start; j < end; ++j) {
                auto& process = m_renderProcesses[j];
                sched->wait(process);

                ++m_passesRendered;
                ++m_passesRenderedThisIter;
                ++passesRenderedLocal;

                int progress = 0;
                bool shouldAbort;
                switch (m_budgetType) {
                    case ESpp:
                        progress = m_passesRendered;
                        shouldAbort = false;
                        break;
                    case ESeconds:
                        progress = (int)computeElapsedSeconds(m_startTime);
                        shouldAbort = progress > m_budget;
                        break;
                    default:
                        Assert(false);
                        break;
                }

                m_progress->update(progress);

                if (process->getReturnStatus() != ParallelProcess::ESuccess) {
                    result = false;
                    shouldAbort = true;
                }

                if (shouldAbort) {
                    goto l_abort;
                }
            }
        }
    l_abort:

        for (auto& process : m_renderProcesses) {
            sched->cancel(process);
        }

        m_renderProcesses.clear();

        variance = 0;
        Bitmap* squaredImage = m_squaredImage->getBitmap();
        Bitmap* image = m_image->getBitmap();

        if (m_sampleCombination == ESampleCombination::EInverseVariance) {
            // Record all previously rendered iterations such that later on all iterations can be
            // combined by weighting them by their estimated inverse pixel variance.
            m_images.push_back(image->clone());
        }

        m_varianceBuffer->clear();

        int N = passesRenderedLocal * m_sppPerPass;

        Vector2i size = squaredImage->getSize();
        for (int x = 0; x < size.x; ++x)
            for (int y = 0; y < size.y; ++y) {
                Point2i pos = Point2i(x, y);
                Spectrum pixel = image->getPixel(pos);
                Spectrum localVar = squaredImage->getPixel(pos) - pixel * pixel / (Float)N;
                image->setPixel(pos, localVar);
                // The local variance is clamped such that fireflies don't cause crazily unstable estimates.
                variance += std::min(localVar.getLuminance(), 10000.0f);
            }

        variance /= (Float)size.x * size.y * (N - 1);
        m_varianceBuffer->put(m_image);

        if (m_sampleCombination == ESampleCombination::EInverseVariance) {
            // Record estimated mean pixel variance for later use in weighting of all images.
            m_variances.push_back(variance);
        }

        Float seconds = computeElapsedSeconds(start);

        const Float ttuv = seconds * variance;
        const Float stuv = passesRenderedLocal * m_sppPerPass * variance;
        Log(EInfo, "%.2f seconds, Total passes: %d, Var: %f, TTUV: %f, STUV: %f.",
            seconds, m_passesRendered, variance, ttuv, stuv);

        return result;
    }

    bool doNeeWithSpp(int spp) {
        switch (m_nee) {
            case ENever:
                return false;
            case EKickstart:
                return spp < 128;
                return true;
            default:
                return true;
        }
    }

    bool renderSPP(Scene *scene, RenderQueue *queue, const RenderJob *job,
        int sceneResID, int sensorResID, int samplerResID, int integratorResID) {

        ref<Scheduler> sched = Scheduler::getInstance();

        size_t sampleCount = (size_t)m_budget;

        ref<Sensor> sensor = static_cast<Sensor *>(sched->getResource(sensorResID));
        ref<Film> film = sensor->getFilm();

        int nPasses = (int)std::ceil(sampleCount / (Float)m_sppPerPass);
        sampleCount = m_sppPerPass * nPasses;

        bool result = true;
        Float currentVarAtEnd = std::numeric_limits<Float>::infinity();

        m_progress = std::unique_ptr<ProgressReporter>(new ProgressReporter("Rendering", nPasses, job));

        while (result && m_passesRendered < nPasses) {
            const int sppRendered = m_passesRendered * m_sppPerPass;
            m_doNee = doNeeWithSpp(sppRendered);

            int remainingPasses = nPasses - m_passesRendered;
            int passesThisIteration = std::min(remainingPasses, 1 << m_iter);

            // If the next iteration does not manage to double the number of passes once more
            // then it would be unwise to throw away the current iteration. Instead, extend
            // the current iteration to the end.
            // This condition can also be interpreted as: the last iteration must always use
            // at _least_ half the total sample budget.
            if (remainingPasses - passesThisIteration < 2 * passesThisIteration) {
                passesThisIteration = remainingPasses;
            }

            Log(EInfo, "ITERATION %d, %d passes", m_iter, passesThisIteration);

            m_isFinalIter = passesThisIteration >= remainingPasses;

            film->clear();
            resetSDTree();

            Float variance;
            if (!performRenderPasses(variance, passesThisIteration, scene, queue, job, sceneResID, sensorResID, samplerResID, integratorResID)) {
                result = false;
                break;
            }

            const Float lastVarAtEnd = currentVarAtEnd;
            currentVarAtEnd = passesThisIteration * variance / remainingPasses;

            Log(EInfo,
                "Extrapolated var:\n"
                "  Last:    %f\n"
                "  Current: %f\n",
                lastVarAtEnd, currentVarAtEnd);

            remainingPasses -= passesThisIteration;
            if (m_sampleCombination == ESampleCombination::EDiscardWithAutomaticBudget && remainingPasses > 0 && (
                    // if there is any time remaining we want to keep going if
                    // either will have less time next iter
                    remainingPasses < passesThisIteration ||
                    // or, according to the convergence behavior, we're better off if we keep going
                    // (we only trust the variance if we drew enough samples for it to be a reliable estimate,
                    // captured by an arbitraty threshold).
                    (sppRendered > 256 && currentVarAtEnd > lastVarAtEnd)
                )) {
                Log(EInfo, "FINAL %d passes", remainingPasses);
                m_isFinalIter = true;
                if (!performRenderPasses(variance, remainingPasses, scene, queue, job, sceneResID, sensorResID, samplerResID, integratorResID)) {
                    result = false;
                    break;
                }
            }
            buildSDTree(scene);

            if (m_dumpSDTree) {
                dumpSDTree(scene, sensor);
            }

            ++m_iter;
            m_passesRenderedThisIter = 0;
        }

        return result;
    }

    static Float computeElapsedSeconds(std::chrono::steady_clock::time_point start) {
        auto current = std::chrono::steady_clock::now();
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(current - start);
        return (Float)ms.count() / 1000;
    }

    bool renderTime(Scene *scene, RenderQueue *queue, const RenderJob *job,
        int sceneResID, int sensorResID, int samplerResID, int integratorResID) {

        ref<Scheduler> sched = Scheduler::getInstance();
        ref<Sensor> sensor = static_cast<Sensor *>(sched->getResource(sensorResID));
        ref<Film> film = sensor->getFilm();

        Float nSeconds = m_budget;

        bool result = true;
        Float currentVarAtEnd = std::numeric_limits<Float>::infinity();

        m_progress = std::unique_ptr<ProgressReporter>(new ProgressReporter("Rendering", (int)nSeconds, job));

        Float elapsedSeconds = 0;

        while (result && elapsedSeconds < nSeconds) {
            const int sppRendered = m_passesRendered * m_sppPerPass;
            m_doNee = doNeeWithSpp(sppRendered);

            Float remainingTime = nSeconds - elapsedSeconds;
            const int passesThisIteration = 1 << m_iter;

            Log(EInfo, "ITERATION %d, %d passes", m_iter, passesThisIteration);

            const auto startIter = std::chrono::steady_clock::now();

            film->clear();
            resetSDTree();

            Float variance;
            if (!performRenderPasses(variance, passesThisIteration, scene, queue, job, sceneResID, sensorResID, samplerResID, integratorResID)) {
                result = false;
                break;
            }

            const Float secondsIter = computeElapsedSeconds(startIter);

            const Float lastVarAtEnd = currentVarAtEnd;
            currentVarAtEnd = secondsIter * variance / remainingTime;

            Log(EInfo,
                "Extrapolated var:\n"
                "  Last:    %f\n"
                "  Current: %f\n",
                lastVarAtEnd, currentVarAtEnd);

            remainingTime -= secondsIter;
            if (m_sampleCombination == ESampleCombination::EDiscardWithAutomaticBudget && remainingTime > 0 && (
                    // if there is any time remaining we want to keep going if
                    // either will have less time next iter
                    remainingTime < secondsIter ||
                    // or, according to the convergence behavior, we're better off if we keep going
                    // (we only trust the variance if we drew enough samples for it to be a reliable estimate,
                    // captured by an arbitraty threshold).
                    (sppRendered > 256 && currentVarAtEnd > lastVarAtEnd)
                )) {
                Log(EInfo, "FINAL %f seconds", remainingTime);
                m_isFinalIter = true;
                do {
                    if (!performRenderPasses(variance, passesThisIteration, scene, queue, job, sceneResID, sensorResID, samplerResID, integratorResID)) {
                        result = false;
                        break;
                    }

                    elapsedSeconds = computeElapsedSeconds(m_startTime);
                } while (elapsedSeconds < nSeconds);
            }
            buildSDTree(scene);

            if (m_dumpSDTree) {
                dumpSDTree(scene, sensor);
            }

            ++m_iter;
            m_passesRenderedThisIter = 0;
            elapsedSeconds = computeElapsedSeconds(m_startTime);
        }

        return result;
    }

    bool render(Scene *scene, RenderQueue *queue, const RenderJob *job,
        int sceneResID, int sensorResID, int samplerResID) {

        m_sdTree = std::unique_ptr<STree>(new STree(scene->getAABB()));
        m_iter = 0;
        m_isFinalIter = false;

        ref<Scheduler> sched = Scheduler::getInstance();

        size_t nCores = sched->getCoreCount();

        for (size_t i = 0; i < nCores; i++) {
            m_prodDTrees.push_back(new DTreeWrapper());
        }

        ref<Sensor> sensor = static_cast<Sensor *>(sched->getResource(sensorResID));
        ref<Film> film = sensor->getFilm();

        auto properties = Properties("hdrfilm");
        properties.setInteger("width", film->getSize().x);
        properties.setInteger("height", film->getSize().y);
        m_varianceBuffer = static_cast<Film*>(PluginManager::getInstance()->createObject(MTS_CLASS(Film), properties));
        m_varianceBuffer->setDestinationFile(scene->getDestinationFile(), 0);

        m_squaredImage = new ImageBlock(Bitmap::ESpectrumAlphaWeight, film->getCropSize());
        m_image = new ImageBlock(Bitmap::ESpectrumAlphaWeight, film->getCropSize());

        m_images.clear();
        m_variances.clear();

        Log(EInfo, "Starting render job (%ix%i, " SIZE_T_FMT " %s, " SSE_STR ") ..", film->getCropSize().x, film->getCropSize().y, nCores, nCores == 1 ? "core" : "cores");

        Thread::initializeOpenMP(nCores);

        int integratorResID = sched->registerResource(this);
        bool result = true;

        Log(EInfo, "Precomputing BSDFs ..");
        precomputeBsdfs(scene, m_gridSideLength);

        m_startTime = std::chrono::steady_clock::now();

        m_passesRendered = 0;
        switch (m_budgetType) {
            case ESpp:
                result = renderSPP(scene, queue, job, sceneResID, sensorResID, samplerResID, integratorResID);
                break;
            case ESeconds:
                result = renderTime(scene, queue, job, sceneResID, sensorResID, samplerResID, integratorResID);
                break;
            default:
                Assert(false);
                break;
        }

        sched->unregisterResource(integratorResID);

        m_progress = nullptr;

        if (m_sampleCombination == ESampleCombination::EInverseVariance) {
            // Combine the last 4 images according to their inverse variance
            film->clear();
            ref<ImageBlock> tmp = new ImageBlock(Bitmap::ESpectrum, film->getCropSize());
            size_t begin = m_images.size() - std::min(m_images.size(), (size_t)4);

            Float totalWeight = 0;
            for (size_t i = begin; i < m_variances.size(); ++i) {
                totalWeight += 1.0f / m_variances[i];
            }

            for (size_t i = begin; i < m_images.size(); ++i) {
                m_images[i]->convert(tmp->getBitmap(), 1.0f / m_variances[i] / totalWeight);
                film->addBitmap(tmp->getBitmap());
            }
        }

        const auto statsFilename =
            "stats-" + scene->getSourceFile().filename().string() + ".json";
        IntegratorStats stats;
        stats.runtime = computeElapsedSeconds(m_startTime) / 60.0f;
        stats.spp = m_passesRendered * m_sppPerPass;
        stats.avgPathLength =
            (float)avgPathLength.getValue() / (float)avgPathLength.getBase();
        stats.percGuiding =
            ((float)percGuiding.getValue() / (float)percGuiding.getBase()) *
            100.0f;
        stats.percNonzero = ((float)percNonzero.getValue() / (float)percNonzero.getBase()) * 100.0f;
        stats.totDtBuildProd = (float)avgDtBuildProd.getValue();
        stats.avgDtBuildProd = (float)avgDtBuildProd.getValue() / (float)avgDtBuildProd.getBase();
        stats.serialize(statsFilename);

        return result;
    }

    void renderBlock(const Scene *scene, const Sensor *sensor,
        Sampler *sampler, ImageBlock *block, const bool &stop,
        const std::vector< TPoint2<uint8_t> > &points) const {

        Float diffScaleFactor = 1.0f /
            std::sqrt((Float)m_sppPerPass);

        bool needsApertureSample = sensor->needsApertureSample();
        bool needsTimeSample = sensor->needsTimeSample();

        RadianceQueryRecord rRec(scene, sampler);
        Point2 apertureSample(0.5f);
        Float timeSample = 0.5f;
        RayDifferential sensorRay;

        block->clear();

        ref<ImageBlock> squaredBlock = new ImageBlock(block->getPixelFormat(), block->getSize(), block->getReconstructionFilter());
        squaredBlock->setOffset(block->getOffset());
        squaredBlock->clear();

        uint32_t queryType = RadianceQueryRecord::ESensorRay;

        if (!sensor->getFilm()->hasAlpha()) // Don't compute an alpha channel if we don't have to
            queryType &= ~RadianceQueryRecord::EOpacity;

        for (size_t i = 0; i < points.size(); ++i) {
            Point2i offset = Point2i(points[i]) + Vector2i(block->getOffset());
            if (stop)
                break;

            for (int j = 0; j < m_sppPerPass; j++) {
                rRec.newQuery(queryType, sensor->getMedium());
                Point2 samplePos(Point2(offset) + Vector2(rRec.nextSample2D()));

                if (needsApertureSample)
                    apertureSample = rRec.nextSample2D();
                if (needsTimeSample)
                    timeSample = rRec.nextSample1D();

                Spectrum spec = sensor->sampleRayDifferential(
                    sensorRay, samplePos, apertureSample, timeSample);

                sensorRay.scaleDifferential(diffScaleFactor);

                spec *= Li(sensorRay, rRec);
                block->put(samplePos, spec, rRec.alpha);
                squaredBlock->put(samplePos, spec * spec, rRec.alpha);
                sampler->advance();
            }
        }

        m_squaredImage->put(squaredBlock);
        m_image->put(block);
    }

    void cancel() {
        const auto& scheduler = Scheduler::getInstance();
        for (size_t i = 0; i < m_renderProcesses.size(); ++i) {
            scheduler->cancel(m_renderProcesses[i]);
        }
    }

    Spectrum sampleMat(const BSDF* bsdf, BSDFSamplingRecord& bRec, Float& woPdf, Float& bsdfPdf, Float& dTreePdf, Float bsdfSamplingFraction, RadianceQueryRecord& rRec, const DTreeWrapper* dTree) const {
        Point2 sample = rRec.nextSample2D();

        auto type = bsdf->getType();
        if (!m_isBuilt || !dTree || (type & BSDF::EDelta) == (type & BSDF::EAll)) {
            auto result = bsdf->sample(bRec, bsdfPdf, sample);
            woPdf = bsdfPdf;
            dTreePdf = 0;
            return result;
        }

        Spectrum result;
        if (sample.x < bsdfSamplingFraction) {
            sample.x /= bsdfSamplingFraction;
            result = bsdf->sample(bRec, bsdfPdf, sample);
            if (result.isZero()) {
                woPdf = bsdfPdf = dTreePdf = 0;
                return Spectrum{0.0f};
            }

            // If we sampled a delta component, then we have a 0 probability
            // of sampling that direction via guiding, thus we can return early.
            if (bRec.sampledType & BSDF::EDelta) {
                dTreePdf = 0;
                woPdf = bsdfPdf * bsdfSamplingFraction;
                return result / bsdfSamplingFraction;
            }

            result *= bsdfPdf;
        } else {
            sample.x = (sample.x - bsdfSamplingFraction) / (1 - bsdfSamplingFraction);
            bRec.wo = bRec.its.toLocal(dTree->sample(rRec.sampler));
            result = bsdf->eval(bRec);
        }

        pdfMat(woPdf, bsdfPdf, dTreePdf, bsdfSamplingFraction, bsdf, bRec, dTree);
        if (woPdf == 0) {
            return Spectrum{0.0f};
        }

        return result / woPdf;
    }

    Spectrum sampleMat(const BSDF* bsdf, BSDFSamplingRecord& bRec, Float& woPdf, Float& bsdfPdf, Float& dTreePdf, Float bsdfSamplingFraction, RadianceQueryRecord& rRec, const DTreeWrapper* gridWrapper, const DTreeWrapper* quadtreeWrapper) const {
        Point2 sample = rRec.nextSample2D();

        auto type = bsdf->getType();
        if (!m_isBuilt || !quadtreeWrapper || (type & BSDF::EDelta) == (type & BSDF::EAll)) {
            auto result = bsdf->sample(bRec, bsdfPdf, sample);
            woPdf = bsdfPdf;
            dTreePdf = 0;
            return result;
        }

        percGuiding.incrementBase();
        Spectrum result;
        if (sample.x < bsdfSamplingFraction) {
            sample.x /= bsdfSamplingFraction;
            result = bsdf->sample(bRec, bsdfPdf, sample);
            if (result.isZero()) {
                woPdf = bsdfPdf = dTreePdf = 0;
                return Spectrum{0.0f};
            }

            // If we sampled a delta component, then we have a 0 probability
            // of sampling that direction via guiding, thus we can return early.
            if (bRec.sampledType & BSDF::EDelta) {
                dTreePdf = 0;
                woPdf = bsdfPdf * bsdfSamplingFraction;
                return result / bsdfSamplingFraction;
            }

            result *= bsdfPdf;
        } else {
            ++percGuiding;
            sample.x = (sample.x - bsdfSamplingFraction) / (1 - bsdfSamplingFraction);
            bRec.wo = bRec.its.toLocal(DTreeWrapper::sample(rRec.sampler, gridWrapper, quadtreeWrapper));
            result = bsdf->eval(bRec);
        }

        pdfMat(woPdf, bsdfPdf, dTreePdf, bsdfSamplingFraction, bsdf, bRec, gridWrapper, quadtreeWrapper);
        if (woPdf == 0) {
            return Spectrum{0.0f};
        }

        return result / woPdf;
    }

    void pdfMat(Float& woPdf, Float& bsdfPdf, Float& dTreePdf, Float bsdfSamplingFraction, const BSDF* bsdf, const BSDFSamplingRecord& bRec, const DTreeWrapper* dTree) const {
        dTreePdf = 0;

        auto type = bsdf->getType();
        if (!m_isBuilt || !dTree || (type & BSDF::EDelta) == (type & BSDF::EAll)) {
            woPdf = bsdfPdf = bsdf->pdf(bRec);
            return;
        }

        bsdfPdf = bsdf->pdf(bRec);
        if (!std::isfinite(bsdfPdf)) {
            woPdf = 0;
            return;
        }

        dTreePdf = dTree->pdf(bRec.its.toWorld(bRec.wo));
        woPdf = bsdfSamplingFraction * bsdfPdf + (1 - bsdfSamplingFraction) * dTreePdf;
    }

    void pdfMat(Float& woPdf, Float& bsdfPdf, Float& dTreePdf, Float bsdfSamplingFraction, const BSDF* bsdf, const BSDFSamplingRecord& bRec, const DTreeWrapper* gridWrapper, const DTreeWrapper* quadtreeWrapper) const {
        dTreePdf = 0;

            auto type = bsdf->getType();
            if (!m_isBuilt || !quadtreeWrapper || (type & BSDF::EDelta) == (type & BSDF::EAll)) {
                woPdf = bsdfPdf = bsdf->pdf(bRec);
                return;
            }

            bsdfPdf = bsdf->pdf(bRec);
            if (!std::isfinite(bsdfPdf)) {
                woPdf = 0;
                return;
            }

            dTreePdf = DTreeWrapper::pdf(bRec.its.toWorld(bRec.wo), gridWrapper, quadtreeWrapper);
            woPdf = bsdfSamplingFraction * bsdfPdf + (1 - bsdfSamplingFraction) * dTreePdf;
    }

    Spectrum Li(const RayDifferential &r, RadianceQueryRecord &rRec) const {
        struct Vertex {
            DTreeWrapper* dTree;
            Vector dTreeVoxelSize;
            Ray ray;

            Spectrum throughput;
            Spectrum bsdfVal;

            Spectrum radiance;

            Float woPdf, bsdfPdf, dTreePdf;
            bool isDelta;

            void record(const Spectrum& r) {
                radiance += r;
            }

            void commit(STree& sdTree, Float statisticalWeight, ESpatialFilter spatialFilter, EDirectionalFilter directionalFilter, EBsdfSamplingFractionLoss bsdfSamplingFractionLoss, Sampler* sampler) {
                if (!(woPdf > 0) || !radiance.isValid() || !bsdfVal.isValid()) {
                    return;
                }

                Spectrum localRadiance = Spectrum{0.0f};
                if (throughput[0] * woPdf > Epsilon) localRadiance[0] = radiance[0] / throughput[0];
                if (throughput[1] * woPdf > Epsilon) localRadiance[1] = radiance[1] / throughput[1];
                if (throughput[2] * woPdf > Epsilon) localRadiance[2] = radiance[2] / throughput[2];
                Spectrum product = localRadiance * bsdfVal;

                DTreeRecord rec{ ray.d, localRadiance.average(), product.average(), woPdf, bsdfPdf, dTreePdf, statisticalWeight, isDelta };
                switch (spatialFilter) {
                    case ESpatialFilter::ENearest:
                        dTree->record(rec, directionalFilter, bsdfSamplingFractionLoss);
                        break;
                    case ESpatialFilter::EStochasticBox:
                        {
                            // DTreeWrapper* splatDTree = dTree;

                            // Jitter the actual position within the
                            // filter box to perform stochastic filtering.
                            // Vector offset = dTreeVoxelSize;
                            // offset.x *= sampler->next1D() - 0.5f;
                            // offset.y *= sampler->next1D() - 0.5f;
                            // offset.z *= sampler->next1D() - 0.5f;

                            // FIXME: Compilation error
                            //Point origin = sdTree.aabb().clip(ray.o + offset);
                            //splatDTree = sdTree.dTreeWrapper(origin);
                            //if (splatDTree) {
                            //    splatDTree->record(rec, directionalFilter, bsdfSamplingFractionLoss);
                            //}
                            break;
                        }
                    case ESpatialFilter::EBox:
                        sdTree.record(ray.o, dTreeVoxelSize, rec, directionalFilter, bsdfSamplingFractionLoss);
                        break;
                }
            }
        };

        static const int MAX_NUM_VERTICES = 32;
        std::array<Vertex, MAX_NUM_VERTICES> vertices;

        /* Some aliases and local variables */
        const Scene *scene = rRec.scene;
        Intersection &its = rRec.its;
        MediumSamplingRecord mRec;
        RayDifferential ray(r);
        Spectrum Li(0.0f);
        Float eta = 1.0f;

        /* Perform the first ray intersection (or ignore if the
        intersection has already been provided). */
        rRec.rayIntersect(ray);

        Spectrum throughput(1.0f);
        bool scattered = false;

        int nVertices = 0;

        auto recordRadiance = [&](Spectrum radiance) {
            Li += radiance;
            for (int i = 0; i < nVertices; ++i) {
                vertices[i].record(radiance);
            }
        };

        DTreeWrapper* prodDTree = m_prodDTrees[Worker::getThread()->getCoreAffinity()];

        while (rRec.depth <= m_maxDepth || m_maxDepth < 0) {

            /* ==================================================================== */
            /*                 Radiative Transfer Equation sampling                 */
            /* ==================================================================== */
            if (rRec.medium && rRec.medium->sampleDistance(Ray(ray, 0, its.t), mRec, rRec.sampler)) {
                /* Sample the integral
                \int_x^y tau(x, x') [ \sigma_s \int_{S^2} \rho(\omega,\omega') L(x,\omega') d\omega' ] dx'
                */
                const PhaseFunction *phase = mRec.getPhaseFunction();

                if (rRec.depth >= m_maxDepth && m_maxDepth != -1) // No more scattering events allowed
                    break;

                throughput *= mRec.sigmaS * mRec.transmittance / mRec.pdfSuccess;

                /* ==================================================================== */
                /*                          Luminaire sampling                          */
                /* ==================================================================== */

                /* Estimate the single scattering component if this is requested */
                DirectSamplingRecord dRec(mRec.p, mRec.time);

                if (rRec.type & RadianceQueryRecord::EDirectMediumRadiance) {
                    int interactions = m_maxDepth - rRec.depth - 1;

                    Spectrum value = scene->sampleAttenuatedEmitterDirect(
                        dRec, rRec.medium, interactions,
                        rRec.nextSample2D(), rRec.sampler);

                    if (!value.isZero()) {
                        const Emitter *emitter = static_cast<const Emitter *>(dRec.object);

                        /* Evaluate the phase function */
                        PhaseFunctionSamplingRecord pRec(mRec, -ray.d, dRec.d);
                        Float phaseVal = phase->eval(pRec);

                        if (phaseVal != 0) {
                            /* Calculate prob. of having sampled that direction using
                            phase function sampling */
                            Float phasePdf = (emitter->isOnSurface() && dRec.measure == ESolidAngle)
                                ? phase->pdf(pRec) : (Float) 0.0f;

                            /* Weight using the power heuristic */
                            const Float weight = miWeight(dRec.pdf, phasePdf);
                            recordRadiance(throughput * value * phaseVal * weight);
                        }
                    }
                }

                /* ==================================================================== */
                /*                         Phase function sampling                      */
                /* ==================================================================== */

                Float phasePdf;
                PhaseFunctionSamplingRecord pRec(mRec, -ray.d);
                Float phaseVal = phase->sample(pRec, phasePdf, rRec.sampler);
                if (phaseVal == 0)
                    break;
                throughput *= phaseVal;

                /* Trace a ray in this direction */
                ray = Ray(mRec.p, pRec.wo, ray.time);
                ray.mint = 0;

                Spectrum value(0.0f);
                rayIntersectAndLookForEmitter(scene, rRec.sampler, rRec.medium,
                    m_maxDepth - rRec.depth - 1, ray, its, dRec, value);

                /* If a luminaire was hit, estimate the local illumination and
                weight using the power heuristic */
                if (!value.isZero() && (rRec.type & RadianceQueryRecord::EDirectMediumRadiance)) {
                    const Float emitterPdf = scene->pdfEmitterDirect(dRec);
                    recordRadiance(throughput * value * miWeight(phasePdf, emitterPdf));
                }

                /* ==================================================================== */
                /*                         Multiple scattering                          */
                /* ==================================================================== */

                /* Stop if multiple scattering was not requested */
                if (!(rRec.type & RadianceQueryRecord::EIndirectMediumRadiance))
                    break;
                rRec.type = RadianceQueryRecord::ERadianceNoEmission;

                if (rRec.depth++ >= m_rrDepth) {
                    /* Russian roulette: try to keep path weights equal to one,
                    while accounting for the solid angle compression at refractive
                    index boundaries. Stop with at least some probability to avoid
                    getting stuck (e.g. due to total internal reflection) */

                    Float q = std::min(throughput.max() * eta * eta, (Float) 0.95f);
                    if (rRec.nextSample1D() >= q)
                        break;
                    throughput /= q;
                }
            } else {
                /* Sample
                tau(x, y) (Surface integral). This happens with probability mRec.pdfFailure
                Account for this and multiply by the proper per-color-channel transmittance.
                */
                if (rRec.medium)
                    throughput *= mRec.transmittance / mRec.pdfFailure;

                if (!its.isValid()) {
                    /* If no intersection could be found, possibly return
                    attenuated radiance from a background luminaire */
                    if ((rRec.type & RadianceQueryRecord::EEmittedRadiance)
                        && (!m_hideEmitters || scattered)) {
                        Spectrum value = throughput * scene->evalEnvironment(ray);
                        if (rRec.medium)
                            value *= rRec.medium->evalTransmittance(ray, rRec.sampler);
                        recordRadiance(value);
                    }

                    break;
                }

                /* Possibly include emitted radiance if requested */
                if (its.isEmitter() && (rRec.type & RadianceQueryRecord::EEmittedRadiance)
                    && (!m_hideEmitters || scattered))
                    recordRadiance(throughput * its.Le(-ray.d));

                /* Include radiance from a subsurface integrator if requested */
                if (its.hasSubsurface() && (rRec.type & RadianceQueryRecord::ESubsurfaceRadiance))
                    recordRadiance(throughput * its.LoSub(scene, rRec.sampler, -ray.d, rRec.depth));

                if (rRec.depth >= m_maxDepth && m_maxDepth != -1)
                    break;

                /* Prevent light leaks due to the use of shading normals */
                Float wiDotGeoN = -dot(its.geoFrame.n, ray.d),
                    wiDotShN = Frame::cosTheta(its.wi);
                if (wiDotGeoN * wiDotShN < 0 && m_strictNormals)
                    break;

                const BSDF *bsdf = its.getBSDF();

                Vector dTreeVoxelSize;
                DTreeWrapper* dTree = nullptr;

                // We only guide smooth BRDFs for now. Analytic product sampling
                // would be conceivable for discrete decisions such as refraction vs
                // reflection.
                const auto bsdf_type = bsdf->getType();

                if (bsdf_type & BSDF::ESmooth) {
                    dTree = m_sdTree->dTreeWrapper(its.p, dTreeVoxelSize);
                }

                Float bsdfSamplingFraction = m_bsdfSamplingFraction;
                if (dTree && m_bsdfSamplingFractionLoss != EBsdfSamplingFractionLoss::ENone) {
                    bsdfSamplingFraction = dTree->bsdfSamplingFraction();
                }

                /* ==================================================================== */
                /*                            BSDF sampling                             */
                /* ==================================================================== */

                /* Sample BSDF * cos(theta) */
                BSDFSamplingRecord bRec(its, rRec.sampler, ERadiance);

                const bool useProdDTree = m_enableProductIs && ((bsdf_type & BSDF::EDelta) != (bsdf_type & BSDF::EAll));
                // Disable product PMF construction if the BSDF is purely specular or purely diffuse
                // const bool useProdDTree = m_enableProductIs && ((bsdf_type & BSDF::EDelta) != (bsdf_type & BSDF::EAll)) && ((bsdf_type & BSDF::EDiffuse) != (bsdf_type & BSDF::EAll));

                // Build product tree
                if (dTree && useProdDTree) {
                    const auto tStart = std::chrono::high_resolution_clock::now();
                    dTree->build_prod(prodDTree, rRec.sampler, bsdf, bRec);
                    const auto tEnd = std::chrono::high_resolution_clock::now();
                    const auto dt = std::chrono::duration_cast<std::chrono::nanoseconds>(tEnd - tStart).count();
                    avgDtBuildProd.incrementBase();
                    avgDtBuildProd += dt;
                }

                Float woPdf, bsdfPdf, dTreePdf;
                Spectrum bsdfWeight = Spectrum(0.0f);
                if (useProdDTree) {
                   bsdfWeight = sampleMat(bsdf, bRec, woPdf, bsdfPdf, dTreePdf, bsdfSamplingFraction, rRec, prodDTree, dTree);
                } else {
                    bsdfWeight = sampleMat(bsdf, bRec, woPdf, bsdfPdf, dTreePdf, bsdfSamplingFraction, rRec, dTree, dTree);
                }

                /* ==================================================================== */
                /*                          Luminaire sampling                          */
                /* ==================================================================== */

                DirectSamplingRecord dRec(its);

                /* Estimate the direct illumination if this is requested */
                if (m_doNee &&
                    (rRec.type & RadianceQueryRecord::EDirectSurfaceRadiance) &&
                    (bsdf->getType() & BSDF::ESmooth)) {
                    int interactions = m_maxDepth - rRec.depth - 1;

                    Spectrum value = scene->sampleAttenuatedEmitterDirect(
                        dRec, its, rRec.medium, interactions,
                        rRec.nextSample2D(), rRec.sampler);

                    if (!value.isZero()) {
                        BSDFSamplingRecord bRec(its, its.toLocal(dRec.d));

                        Float woDotGeoN = dot(its.geoFrame.n, dRec.d);

                        /* Prevent light leaks due to the use of shading normals */
                        if (!m_strictNormals || woDotGeoN * Frame::cosTheta(bRec.wo) > 0) {
                            /* Evaluate BSDF * cos(theta) */
                            const Spectrum bsdfVal = bsdf->eval(bRec);

                            /* Calculate prob. of having generated that direction using BSDF sampling */
                            const Emitter *emitter = static_cast<const Emitter *>(dRec.object);
                            Float woPdf = 0, bsdfPdf = 0, dTreePdf = 0;
                            if (emitter->isOnSurface() && dRec.measure == ESolidAngle) {
                                if (useProdDTree) {
                                     pdfMat(woPdf, bsdfPdf, dTreePdf, bsdfSamplingFraction, bsdf, bRec, prodDTree, dTree);
                                } else {
                                    pdfMat(woPdf, bsdfPdf, dTreePdf, bsdfSamplingFraction, bsdf, bRec, dTree, dTree);
                                }
                            }

                            /* Weight using the power heuristic */
                            const Float weight = miWeight(dRec.pdf, woPdf);

                            value *= bsdfVal;
                            Spectrum L = throughput * value * weight;

                            if (!m_isFinalIter && m_nee != EAlways) {
                                if (dTree) {
                                    Vertex v = Vertex{
                                        dTree,
                                        dTreeVoxelSize,
                                        Ray(its.p, dRec.d, 0),
                                        (throughput * bsdfVal) / dRec.pdf, // throughput * bsdfVal * dRec.pdf,
                                        bsdfVal,
                                        L,
                                        dRec.pdf,
                                        bsdfPdf,
                                        dTreePdf,
                                        false,
                                    };

                                    v.commit(*m_sdTree, 0.5f, m_spatialFilter, m_directionalFilter, m_isBuilt ? m_bsdfSamplingFractionLoss : EBsdfSamplingFractionLoss::ENone, rRec.sampler);
                                }
                            }

                            recordRadiance(L);
                        }
                    }
                }

                // BSDF handling
                if (bsdfWeight.isZero())
                    break;

                /* Prevent light leaks due to the use of shading normals */
                const Vector wo = its.toWorld(bRec.wo);
                Float woDotGeoN = dot(its.geoFrame.n, wo);

                if (woDotGeoN * Frame::cosTheta(bRec.wo) <= 0 && m_strictNormals)
                    break;

                /* Trace a ray in this direction */
                ray = Ray(its.p, wo, ray.time);

                /* Keep track of the throughput, medium, and relative
                refractive index along the path */
                throughput *= bsdfWeight;
                eta *= bRec.eta;
                if (its.isMediumTransition())
                    rRec.medium = its.getTargetMedium(ray.d);

                /* Handle index-matched medium transitions specially */
                if (bRec.sampledType == BSDF::ENull) {
                    if (!(rRec.type & RadianceQueryRecord::EIndirectSurfaceRadiance))
                        break;

                    // There exist materials that are smooth/null hybrids (e.g. the mask BSDF), which means that
                    // for optimal-sampling-fraction optimization we need to record null transitions for such BSDFs.
                    if (m_bsdfSamplingFractionLoss != EBsdfSamplingFractionLoss::ENone && dTree && nVertices < MAX_NUM_VERTICES && !m_isFinalIter) {
                        if (1 / woPdf > 0) {
                            vertices[nVertices] = Vertex{
                                dTree,
                                dTreeVoxelSize,
                                ray,
                                throughput,
                                bsdfWeight * woPdf,
                                Spectrum{0.0f},
                                woPdf,
                                bsdfPdf,
                                dTreePdf,
                                true,
                            };

                            ++nVertices;
                        }
                    }

                    rRec.type = scattered ? RadianceQueryRecord::ERadianceNoEmission
                        : RadianceQueryRecord::ERadiance;
                    scene->rayIntersect(ray, its);
                    rRec.depth++;
                    continue;
                }

                Spectrum value(0.0f);
                rayIntersectAndLookForEmitter(scene, rRec.sampler, rRec.medium,
                    m_maxDepth - rRec.depth - 1, ray, its, dRec, value);

                /* If a luminaire was hit, estimate the local illumination and
                weight using the power heuristic */
                if (rRec.type & RadianceQueryRecord::EDirectSurfaceRadiance) {
                    bool isDelta = bRec.sampledType & BSDF::EDelta;
                    const Float emitterPdf = (m_doNee && !isDelta && !value.isZero()) ? scene->pdfEmitterDirect(dRec) : 0;

                    const Float weight = miWeight(woPdf, emitterPdf);
                    Spectrum L = throughput * value * weight;

                    if (!L.isZero()) {
                        recordRadiance(L);
                    }

                    if ((!isDelta || m_bsdfSamplingFractionLoss != EBsdfSamplingFractionLoss::ENone) && dTree && nVertices < MAX_NUM_VERTICES && !m_isFinalIter) {
                        if (1 / woPdf > 0) {
                            vertices[nVertices] = Vertex{
                                dTree,
                                dTreeVoxelSize,
                                ray,
                                throughput,
                                bsdfWeight * woPdf,
                                (m_nee == EAlways) ? Spectrum{0.0f} : L,
                                woPdf,
                                bsdfPdf,
                                dTreePdf,
                                isDelta,
                            };

                            ++nVertices;
                        }
                    }
                }

                /* ==================================================================== */
                /*                         Indirect illumination                        */
                /* ==================================================================== */

                /* Stop if indirect illumination was not requested */
                if (!(rRec.type & RadianceQueryRecord::EIndirectSurfaceRadiance))
                    break;

                rRec.type = RadianceQueryRecord::ERadianceNoEmission;

                // Russian roulette
                if (rRec.depth++ >= m_rrDepth) {
                    Float successProb = 1.0f;
                    if (dTree && !(bRec.sampledType & BSDF::EDelta)) {
                        if (!m_isBuilt) {
                            successProb = throughput.max() * eta * eta;
                        } else {
                            // The adjoint russian roulette implementation of Mueller et al. [2017]
                            // was broken, effectively turning off russian roulette entirely.
                            // For reproducibility's sake, we therefore removed adjoint russian roulette
                            // from this codebase rather than fixing it.
                        }

                        successProb = std::max(0.1f, std::min(successProb, 0.99f));
                    }

                    if (rRec.nextSample1D() >= successProb)
                        break;
                    throughput /= successProb;
                }
            }

            scattered = true;
        }
        avgPathLength.incrementBase();
        avgPathLength += rRec.depth;

        if (nVertices > 0 && !m_isFinalIter) {
            for (int i = 0; i < nVertices; ++i) {
                vertices[i].commit(*m_sdTree, m_doNee ? 0.5f : 1.0f, m_spatialFilter, m_directionalFilter, m_isBuilt ? m_bsdfSamplingFractionLoss : EBsdfSamplingFractionLoss::ENone, rRec.sampler);
            }
        }

        percNonzero.incrementBase();

        if (!Li.isZero()) {
            ++percNonzero;
        }

        return Li;
    }

    /**
    * This function is called by the recursive ray tracing above after
    * having sampled a direction from a BSDF/phase function. Due to the
    * way in which this integrator deals with index-matched boundaries,
    * it is necessarily a bit complicated (though the improved performance
    * easily pays for the extra effort).
    *
    * This function
    *
    * 1. Intersects 'ray' against the scene geometry and returns the
    *    *first* intersection via the '_its' argument.
    *
    * 2. It checks whether the intersected shape was an emitter, or if
    *    the ray intersects nothing and there is an environment emitter.
    *    In this case, it returns the attenuated emittance, as well as
    *    a DirectSamplingRecord that can be used to query the hypothetical
    *    sampling density at the emitter.
    *
    * 3. If current shape is an index-matched medium transition, the
    *    integrator keeps on looking on whether a light source eventually
    *    follows after a potential chain of index-matched medium transitions,
    *    while respecting the specified 'maxDepth' limits. It then returns
    *    the attenuated emittance of this light source, while accounting for
    *    all attenuation that occurs on the wya.
    */
    void rayIntersectAndLookForEmitter(const Scene *scene, Sampler *sampler,
        const Medium *medium, int maxInteractions, Ray ray, Intersection &_its,
        DirectSamplingRecord &dRec, Spectrum &value) const {
        Intersection its2, *its = &_its;
        Spectrum transmittance(1.0f);
        bool surface = false;
        int interactions = 0;

        while (true) {
            surface = scene->rayIntersect(ray, *its);

            if (medium)
                transmittance *= medium->evalTransmittance(Ray(ray, 0, its->t), sampler);

            if (surface && (interactions == maxInteractions ||
                !(its->getBSDF()->getType() & BSDF::ENull) ||
                its->isEmitter())) {
                /* Encountered an occluder / light source */
                break;
            }

            if (!surface)
                break;

            if (transmittance.isZero())
                return;

            if (its->isMediumTransition())
                medium = its->getTargetMedium(ray.d);

            Vector wo = its->shFrame.toLocal(ray.d);
            BSDFSamplingRecord bRec(*its, -wo, wo, ERadiance);
            bRec.typeMask = BSDF::ENull;
            transmittance *= its->getBSDF()->eval(bRec, EDiscrete);

            ray.o = ray(its->t);
            ray.mint = Epsilon;
            its = &its2;

            if (++interactions > 100) { /// Just a precaution..
                Log(EWarn, "rayIntersectAndLookForEmitter(): round-off error issues?");
                return;
            }
        }

        if (surface) {
            /* Intersected something - check if it was a luminaire */
            if (its->isEmitter()) {
                dRec.setQuery(ray, *its);
                value = transmittance * its->Le(-ray.d);
            }
        } else {
            /* Intersected nothing -- perhaps there is an environment map? */
            const Emitter *env = scene->getEnvironmentEmitter();

            if (env && env->fillDirectSamplingRecord(dRec, ray)) {
                value = transmittance * env->evalEnvironment(RayDifferential(ray));
                dRec.dist = std::numeric_limits<Float>::infinity();
                its->t = std::numeric_limits<Float>::infinity();
            }
        }
    }

    Float miWeight(Float pdfA, Float pdfB) const {
        pdfA *= pdfA; pdfB *= pdfB;
        return pdfA / (pdfA + pdfB);
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << "GuidedPathTracer[" << endl
            << "  maxDepth = " << m_maxDepth << "," << endl
            << "  rrDepth = " << m_rrDepth << "," << endl
            << "  strictNormals = " << m_strictNormals << endl
            << "]";
        return oss.str();
    }

private:
    /// The datastructure for guiding paths.
    std::unique_ptr<STree> m_sdTree;
    std::vector<DTreeWrapper*> m_prodDTrees;

    /// The squared values of our currently rendered image. Used to estimate variance.
    mutable ref<ImageBlock> m_squaredImage;
    /// The currently rendered image. Used to estimate variance.
    mutable ref<ImageBlock> m_image;

    std::vector<ref<Bitmap>> m_images;
    std::vector<Float> m_variances;

    /// This contains the currently estimated variance.
    mutable ref<Film> m_varianceBuffer;

    /// The modes of NEE which are supported.
    enum ENee {
        ENever,
        EKickstart,
        EAlways,
    };

    /**
        How to perform next event estimation (NEE). The following values are valid:
        - "never":     Never performs NEE.
        - "kickstart": Performs NEE for the first few iterations to initialize
                       the SDTree with good direct illumination estimates.
        - "always":    Always performs NEE.
        Default = "never"
    */
    std::string m_neeStr;
    ENee m_nee;

    /// Whether Li should currently perform NEE (automatically set during rendering based on m_nee).
    bool m_doNee;

    enum EBudget {
        ESpp,
        ESeconds,
    };

    /**
        What type of budget to use. The following values are valid:
        - "spp":     Budget is the number of samples per pixel.
        - "seconds": Budget is a time in seconds.
        Default = "seconds"
    */
    std::string m_budgetStr;
    EBudget m_budgetType;
    Float m_budget;

    bool m_isBuilt = false;
    int m_iter;
    bool m_isFinalIter = false;

    int m_sppPerPass;

    int m_passesRendered;
    int m_passesRenderedThisIter;
    mutable std::unique_ptr<ProgressReporter> m_progress;

    std::vector<ref<BlockedRenderProcess>> m_renderProcesses;

    /**
        How to combine the samples from all path-guiding iterations:
        - "discard":    Discard all but the last iteration.
        - "automatic":  Discard all but the last iteration, but automatically assign an appropriately
                        larger budget to the last [Mueller et al. 2018].
        - "inversevar": Combine samples of the last 4 iterations based on their
                        mean pixel variance [Mueller et al. 2018].
        Default     = "automatic" (for reproducibility)
        Recommended = "inversevar"
    */
    std::string m_sampleCombinationStr;
    ESampleCombination m_sampleCombination;


    /// Maximum memory footprint of the SDTree in MB. Stops subdividing once reached. -1 to disable.
    int m_sdTreeMaxMemory;

    /**
        The spatial filter to use when splatting radiance samples into the SDTree.
        The following values are valid:
        - "nearest":    No filtering [Mueller et al. 2017].
        - "stochastic": Stochastic box filter; improves upon Mueller et al. [2017]
                        at nearly no computational cost.
        - "box":        Box filter; improves the quality further at significant
                        additional computational cost.
        Default     = "nearest" (for reproducibility)
        Recommended = "stochastic"
    */
    std::string m_spatialFilterStr;
    ESpatialFilter m_spatialFilter;

    /**
        The directional filter to use when splatting radiance samples into the SDTree.
        The following values are valid:
        - "nearest":    No filtering [Mueller et al. 2017].
        - "box":        Box filter; improves upon Mueller et al. [2017]
                        at nearly no computational cost.
        Default     = "nearest" (for reproducibility)
        Recommended = "box"
    */
    std::string m_directionalFilterStr;
    EDirectionalFilter m_directionalFilter;

    /**
        Leaf nodes of the spatial binary tree are subdivided if the number of samples
        they received in the last iteration exceeds c * sqrt(2^k) where c is this value
        and k is the iteration index. The first iteration has k==0.
        Default     = 12000 (for reproducibility)
        Recommended = 4000
    */
    int m_sTreeThreshold;

    /**
        Leaf nodes of the directional quadtree are subdivided if the fraction
        of energy they carry exceeds this value.
        Default = 0.01 (1%)
    */
    Float m_dTreeThreshold;

    /**
        When guiding, we perform MIS with the balance heuristic between the guiding
        distribution and the BSDF, combined with probabilistically choosing one of the
        two sampling methods. This factor controls how often the BSDF is sampled
        vs. how often the guiding distribution is sampled.
        Default = 0.5 (50%)
    */
    Float m_bsdfSamplingFraction;

    /**
        The loss function to use when learning the bsdfSamplingFraction using gradient
        descent, following the theory of Neural Importance Sampling [Mueller et al. 2018].
        The following values are valid:
        - "none":  No learning (uses the fixed `m_bsdfSamplingFraction`).
        - "kl":    Optimizes bsdfSamplingFraction w.r.t. the KL divergence.
        - "var":   Optimizes bsdfSamplingFraction w.r.t. variance.
        Default     = "none" (for reproducibility)
        Recommended = "kl"
    */
    std::string m_bsdfSamplingFractionLossStr;
    EBsdfSamplingFractionLoss m_bsdfSamplingFractionLoss;

    /**
        Whether to dump a binary representation of the SD-Tree to disk after every
        iteration. The dumped SD-Tree can be visualized with the accompanying
        visualizer tool.
        Default = false
    */
    bool m_dumpSDTree;

    int m_gridSideLength;
    bool m_enableProductIs;

    /// The time at which rendering started.
    std::chrono::steady_clock::time_point m_startTime;

public:
    MTS_DECLARE_CLASS()
};

MTS_IMPLEMENT_CLASS(HybridGuidedPathTracer, false, MonteCarloIntegrator)
MTS_EXPORT_PLUGIN(HybridGuidedPathTracer, "Product guided path tracer with grid-quadtree hybrid");
MTS_NAMESPACE_END
