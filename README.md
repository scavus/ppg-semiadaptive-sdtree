## Product path guiding with semi-adaptive spatio-directional tree (Computers & Graphics)<br><sub>Official Implementation </sub>
**[Paper](https://www.sciencedirect.com/science/article/abs/pii/S0097849321002442)**

Abstract: *Monte Carlo integration is an established method in simulating light transport. However, due to its stochastic nature, it requires copious amounts of samples to eliminate the estimation error, and variance reduction techniques such as importance sampling are used to improve the convergence speed as a remedy. Path guiding is a class of adaptive importance sampling methods devised specifically for light transport simulation, which demonstrates significant improvements over classical sampling techniques. Yet most of the previous path guiding methods only account for the radiance term, omitting the bidirectional scattering distribution function (BSDF) term. This results in suboptimal sample quality for non-diffuse light transport. This paper presents a path guiding method which guides paths according to the full product of the BSDF and radiance terms in light transport equation. Extending a spatio-directional tree based path guiding method, we use a semi-adaptive grid-quadtree hybrid subdividing the directional domain in a spatial subdomain to learn and represent the radiance field. With the help of BSDF lookup tables used to accelerate BSDF evaluations, this grid-quadtree allows us to efficiently construct approximate product distributions and guide paths according to the product of incident radiance and cosine-weighted BSDF at each path vertex. The resulting method is relatively simple to implement and enables more robust sampling on scenes with many glossy surfaces.*

![Teaser](./assets/fig1.png)

## Implementation
This implementation is based on the official Mitsuba 0.6 implementation of Practical Path Guiding, which can be found in [this repo](https://github.com/Tom94/practical-path-guiding).
Our proposed integrator is implemented in `src/integrators/path/guided_path_hybrid.cpp`

### Parameters
Our integrator adds several product sampling related parameters to the baseline:

``` xml
<boolean name="enableProductIs" value="true"/>
```

  - Enable/disable product importance sampling. Disabling this effectively turns the integrator into baseline Practical Path Guiding.

``` xml
<integer name="gridSideLength" value="8"/>
```

  - Controls the resolution of the product sampling grid. Should be a power of 2. Setting this value higher than 16 is _not recommended_.

``` xml
<string name="bsdfLutType" value="worldspace"/>
```

  - Determines the type of the BSDF LUT. Can be `objectspace`, `worldspace` or `none`. `objectspace` has a small memory footprint, but requires coordinate transformations during product distribution computation. `worldspace` on the other hand requires no such coordinate transformations, i.e. indexing is efficient, but its space complexity is much higher. `none` disables the BSDF LUT approximation altogether. In this mode, the BSDF is evaluated many times during the product distribution computation. This is more robust but also extremely slow.

``` xml
<boolean name="enableWorldspaceLutCompression" value="true"/>
```

  - Enable/disable compression for world space BSDF LUTs.

``` xml
<integer name="bsdfLutSampleCount" value="4"/>
```

  - Number of samples to take from the BSDF to determine the value of a LUT cell.

``` xml
<boolean name="enableSimd" value="true"/>
```

  - Enable/disable SIMD parallel code path for building the product distribution. Ignored when `bsdfLutType` is `objectspace`.

An example configuration file is provided with the bundled scene. See `scenes/dragon/grid-8x8-worldspace-lut-bsdfavg-simd.xml`.

## Building

To build the code, a GNU/Linux distribution is recommended. We tested the code on Arch Linux.

Mitsuba 0.6 uses an old version of SCons build system that requires Python 2, which has reached EOL and has been deleted from most distributions.
To install the SCons build system, first install [Miniconda](https://docs.conda.io/en/latest/miniconda.html), then run

``` shell
conda create --name=mitsuba-0.6 python=2
conda activate mitsuba-0.6
conda install -c conda-forge scons=3.1.2
```

For compatibility reasons we have also disabled/removed OpenEXR support and the Qt-based frontend `mtsgui`. Any reference to GLEW-MX is also removed and as a result OpenGL-based hardware acceleration may be broken. Please note that we _do not_ utilize any form of hardware acceleration in this project.

To install build dependencies on Arch Linux, run

``` shell
pacman -S boost boost-libs libjpeg libpng glew fftw eigen xerces-c pcre
```

Before starting the build, make sure that you are in the root directory of the repository. If you are building for the first time, copy the build configuration file:

``` shell
cp build/config-linux-gcc.py config.py
```

To start the build, run

``` shell
conda activate mitsuba-0.6
scons
```

## Citation

``` latex
@article{CAVUS2022212,
title = {Product path guiding with semi-adaptive spatio-directional tree},
journal = {Computers \& Graphics},
volume = {103},
pages = {212-222},
year = {2022},
issn = {0097-8493},
doi = {https://doi.org/10.1016/j.cag.2021.11.003},
url = {https://www.sciencedirect.com/science/article/pii/S0097849321002442},
author = {Sencer Çavuş and Mehmet Baran},
keywords = {Path tracing, Importance sampling, Path guiding}
}
```
